package ph.com.applife.myReportNew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import ph.com.applife.myReportNew.reports.Add_Item;
import ph.com.applife.myReportNew.reports.Agent;
import ph.com.applife.myReportNew.reports.Diser;

public class MainActivity extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    private TextView text1,text2;
    Button btn1,btn2,btn3;

    SQLiteHelper SQLITEHELPER;
    SQLiteDatabase SQLITEDATABASE;
    Cursor cursor;
    SQLiteListAdapter ListAdapter ;

    ArrayList<String> ID_ArrayList = new ArrayList<String>();
    ArrayList<String> PRODUCT_ArrayList = new ArrayList<String>();
    ArrayList<String> BEGINNINGINVENTORY_ArrayList = new ArrayList<String>();
    ArrayList<String> EXPIREDATE_ArrayList = new ArrayList<String>();
    ArrayList<String> SOLIDSTOCKS_ArrayList = new ArrayList<String>();
    ArrayList<String> STOCKSPULLOUT_ArrayList = new ArrayList<String>();
    ArrayList<String> STOCKEXPIREDATE_ArrayList = new ArrayList<String>();
    ArrayList<String> DELIVERYITEM_ArrayList = new ArrayList<String>();
    ArrayList<String> ENDINGINVENTORY_ArrayList = new ArrayList<String>();
    ArrayList<String> WAREHOUSE_ArrayList = new ArrayList<String>();
    ArrayList<String> RDUSTOCKS_ArrayList = new ArrayList<String>();
    ArrayList<String> RATBITE_ArrayList = new ArrayList<String>();
    ListView LISTVIEW;
    int arr;
    JSONArray resultSet;
    JSONObject rowObject;
    OutputStream os = null;
    InputStream is = null;
    HttpURLConnection conn = null;
    SessionManager manager;
    private SharedPreference sharedPreference;
    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "font/Gothic.ttf");

        //Declaring List View
        LISTVIEW = (ListView) findViewById(R.id.list);

        //Declaring Sqlite Helper, accessing variable in the database
        SQLITEHELPER = new SQLiteHelper(this);

        manager = new SessionManager();
        sharedPreference = new SharedPreference();

        arr = SQLITEHELPER.numberOfRows();
        SQLITEHELPER.close();

        //Declare Text View
        text1 = (TextView) findViewById(R.id.textView18);
        text1.setTypeface(custom_font);

        btn1 = (Button) findViewById(R.id.button13);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Add_Item.class);
                startActivity(intent);
                MainActivity.this.finish();
            }

        });
        //Declare Button
        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MainActivity.AsyncReport().execute();
            }

        });
        //Declare Button
        btn3 = (Button) findViewById(R.id.button18);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.setPreferences(MainActivity.this, "status", "0");
                Intent intent = new Intent(MainActivity.this, Agent.class);
                startActivity(intent);
                MainActivity.this.finish();
            }

        });

    }
    @Override
    protected void onResume() {

        ShowSQLiteDBdata() ;

        super.onResume();
    }

    private void ShowSQLiteDBdata() {
        //Declaring Sqlite Helper, accessing variable in the database
        SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        // Stating Query
        cursor = SQLITEDATABASE.rawQuery("SELECT * FROM iteminventory", null);

        ID_ArrayList.clear();
        PRODUCT_ArrayList.clear();
        BEGINNINGINVENTORY_ArrayList.clear();
        SOLIDSTOCKS_ArrayList.clear();
        EXPIREDATE_ArrayList.clear();
        STOCKSPULLOUT_ArrayList.clear();
        STOCKEXPIREDATE_ArrayList.clear();
        DELIVERYITEM_ArrayList.clear();
        ENDINGINVENTORY_ArrayList.clear();
        WAREHOUSE_ArrayList.clear();
        RDUSTOCKS_ArrayList.clear();
        RATBITE_ArrayList.clear();

        if (cursor.moveToFirst()) {
            do {
                ID_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_ITEMCODE)));
                PRODUCT_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_PRODUCT)));
                BEGINNINGINVENTORY_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_BEGINNINGINVENTORY)));
                SOLIDSTOCKS_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_SOLIDSTOCKS)));
                EXPIREDATE_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_EXPIRYDATE)));
                STOCKSPULLOUT_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_STOCKSFORPULLOUT)));
                STOCKEXPIREDATE_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_EXPIRYDATEPULLOUT)));
                DELIVERYITEM_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_DELIVERYITEMS)));
                ENDINGINVENTORY_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_ENDINGINVENTORY)));
                WAREHOUSE_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_WAREHOUSE)));
                RDUSTOCKS_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_RDUSTOCKS)));
                RATBITE_ArrayList.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.ITEM_RATBITE)));
            } while (cursor.moveToNext());
        }
        //Placing value inside the list view
        ListAdapter = new SQLiteListAdapter(MainActivity.this,

                ID_ArrayList,
                PRODUCT_ArrayList,
                BEGINNINGINVENTORY_ArrayList,
                SOLIDSTOCKS_ArrayList,
                EXPIREDATE_ArrayList,
                STOCKSPULLOUT_ArrayList,
                STOCKEXPIREDATE_ArrayList,
                DELIVERYITEM_ArrayList,
                ENDINGINVENTORY_ArrayList,
                WAREHOUSE_ArrayList,
                RDUSTOCKS_ArrayList,
                RATBITE_ArrayList
        );

        LISTVIEW.setAdapter(ListAdapter);
        cursor.close();
    }
    private class AsyncReport extends AsyncTask<Object, Object, String> {
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(Object... params) {
            //Selecting values inside the database
            SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
            cursor = SQLITEDATABASE.rawQuery("SELECT itemcode,product,beginninginventory,expirydate,solidstocks,stocksforpullout,expirydatepullout,deliveryitems,endinginventory,warehouse,rdustocks,ratbite FROM iteminventory", null);

            resultSet = new JSONArray();
            cursor.moveToFirst();

            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                rowObject = new JSONObject();
                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {
                        try {
                            if (cursor.getString(i) != null) {
                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {

                                rowObject.put(cursor.getColumnName(i), "");
                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                }
                resultSet.put(rowObject);
                cursor.moveToNext();
            }

            try {
                // Create a new HttpClient and Post Header
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://applife.com.ph/ireport/diser_item.php");
                // Post the data:
                StringEntity se = new StringEntity(resultSet.toString());
                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                // Execute HTTP Post Request
                System.out.print(rowObject);
                HttpResponse response = httpclient.execute(httppost);
                // for JSON:
                if(response != null)
                {
                    is = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    finally {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return ("successful");
                }
            }
            catch (ClientProtocolException e) {
            } catch (IOException e) {
                cursor.close();
            }
            return ("unsuccessful");
        }


        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            pdLoading.dismiss();
            if (result.equals("unsuccessful")){
                //Calling error function
                somethingwentwrong();
            }else if (result.equals("successful")){
                SQLiteDatabase database = SQLITEHELPER.getWritableDatabase();
                database.delete(SQLiteHelper.table_ITEM, null, null);
                success();
            }
        }
    }

    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, Diser.class);
                        startActivity(intent);
                        MainActivity.this.finish();
                    }
                });
        builder.show();
    }

    private void somethingwentwrong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Something when wrong. Please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press back to Dashboard.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
}