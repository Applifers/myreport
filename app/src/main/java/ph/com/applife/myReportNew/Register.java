package ph.com.applife.myReportNew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@SuppressWarnings("ALL")
public class Register extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    private EditText emailadd,name,user,pass,cpass,send,ali;
    String fullname,email,username,password,confirm,position,alias,sendto;
    String valid_email;
    Typeface font;

    public static String  full;
    public static String  friendly_alias;
    public static String  emailsendto;
    public static String  emailfrom;
    public static String  user_;
    private static final String TAG_fullname = "fullname";
    private static final String TAG_email = "email";
    private static final String TAG_username = "username";
    private static final String TAG_sendto = "sendto";
    private static final String TAG_alias = "alias";

    SessionManager manager;
    private SharedPreference sharedPreference;
    Activity context = this;
    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Declare "Gothic" font
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "font/Gothic.ttf");

        manager = new SessionManager();
        sharedPreference = new SharedPreference();

        // Declare Edit Text
        name = (EditText) findViewById(R.id.fullname);
        name.setTypeface(custom_font);
        emailadd = (EditText) findViewById(R.id.email);
        emailadd.setTypeface(custom_font);
        emailadd.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                // TODO Auto-generated method stub
                Is_Valid_Email(emailadd); // pass your EditText Obj here.
            }

            @SuppressWarnings("ConstantConditions")
            public void Is_Valid_Email(EditText edt) {
                //noinspection ConstantConditions
                if (edt.getText().toString() == null) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else if (isEmailValid(edt.getText().toString()) == false) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else {
                    valid_email = edt.getText().toString();
                }
            }
            boolean isEmailValid(CharSequence email) {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches();
            }
        });

        ali = (EditText) findViewById(R.id.alias);
        ali.setTypeface(custom_font);

        send = (EditText) findViewById(R.id.sendto);
        send.setTypeface(custom_font);
        send.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                // TODO Auto-generated method stub
                Is_Valid_Email(send); // pass your EditText Obj here.
            }

            @SuppressWarnings("ConstantConditions")
            public void Is_Valid_Email(EditText edt) {
                //noinspection ConstantConditions
                if (edt.getText().toString() == null) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else if (isEmailValid(edt.getText().toString()) == false) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else {
                    valid_email = edt.getText().toString();
                }
            }

            boolean isEmailValid(CharSequence email) {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches();
            }
        });

        user = (EditText) findViewById(R.id.username);
        user.setTypeface(custom_font);
        pass = (EditText) findViewById(R.id.password);
        pass.setTypeface(custom_font);
        cpass = (EditText) findViewById(R.id.cpassword);
        cpass.setTypeface(custom_font);

    }

    public void goRegister(View arg0){
        //check if my internet connection
        isInternetOn();

        // Objects to String
        fullname = name.getText().toString();
        email = emailadd.getText().toString();
        username = user.getText().toString();
        password = pass.getText().toString();
        confirm=cpass.getText().toString();
        alias = ali.getText().toString();
        sendto= send.getText().toString();

        // Messages if empty
        if(!(password.equals(confirm))) {
            //Check if empty
            completetheform();
        }else if (fullname.equals("")){
            //Check if empty
            completetheform();
        }else if (email.equals("")){
            //Check if empty
            completetheform();
        }else if (username.equals("")){
            //Check if empty
            completetheform();
        }else if (password.equals("")){
            //Check if empty
            completetheform();
        }else if (password.length() < 8){
            //Check if the length of password is eight characters
            minimumof8();
        }else if (confirm.equals("")){
            //Check if empty
            completetheform();
        }else if (alias.equals("")){
            //Check if empty
            completetheform();
        }else if (sendto.equals("")){
            //Check if empty
            completetheform();
        }
        else {
            // Calling the function of AsyncRegister for registering the data from the Register Form
            new AsyncRegister().execute(fullname, email, username, password, alias, sendto);
        }
    }
    private class AsyncRegister extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(Register.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/register.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);
                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("fullname", params[0])
                        .appendQueryParameter("email", params[1])
                        .appendQueryParameter("username", params[2])
                        .appendQueryParameter("password", params[3])
                        .appendQueryParameter("alias", params[4])
                        .appendQueryParameter("sendto", params[5]);
                String query = builder.build().getEncodedQuery();
                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }
            try {
                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return(result.toString());
                }else{
                    return("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }
        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();

            if(result.equalsIgnoreCase("nyay!")) {
                //Calling eaddexist function
                eaddexist();
            }else {

                try {
                    JSONArray jArray = new JSONArray(result);
                    // Extract data from json and store into ArrayList as class objects
                    //
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject c = jArray.getJSONObject(i);
                        fullname = c.getString(TAG_fullname);
                        email = c.getString(TAG_email);
                        username = c.getString(TAG_username);
                        sendto = c.getString(TAG_sendto);
                        alias = c.getString(TAG_alias);
                    }
                    //Declaring json array to another string
                    full = fullname;
                    emailfrom = email;
                    friendly_alias = alias;
                    emailsendto = sendto;
                    user_ = username;

                    manager.setPreferences(Register.this, "status", "1");
                    text=fullname;
                    String status=manager.getPreferences(Register.this,"status");
                    Log.d("status", status);

                    // Save the text in SharedPreference
                    sharedPreference.save(context,text);
                    //Calling function "welcome"
                    welcome();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void welcome() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_dialog_info);
        builder.setMessage("Welcome,You are successfully registered.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Intent intent = new Intent(Register.this,Dashboard.class);
                        startActivity(intent);
                        Register.this.finish();
                    }
                });
        builder.show();
    }


    private void minimumof8() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("The password should consist of 8 characters, please try again. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void eaddexist() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Email address or username exist, please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
//                        Intent intent = new Intent(Register.this,Register.class);
//                        startActivity(intent);
//                        Register.this.finish();
                    }
                });
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please check your Internet connection");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    //Check if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        //Go to Launcher Activity
        Intent mainIntent = new Intent(Register.this,Login.class);
        Register.this.startActivity(mainIntent);
        Register.this.finish();

        super.onBackPressed();
    }
}