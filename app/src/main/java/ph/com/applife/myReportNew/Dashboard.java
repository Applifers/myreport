package ph.com.applife.myReportNew;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;

import ph.com.applife.myReportNew.reports.Agent;
import ph.com.applife.myReportNew.reports.Diser;
import ph.com.applife.myReportNew.reports.GenericForm;
import ph.com.applife.myReportNew.reports.Logistics;
import ph.com.applife.myReportNew.reports.RovingForm;
import ph.com.applife.myReportNew.reports.SalesReport;
import ph.com.applife.myReportNew.reports.SiteLocator;
import ph.com.applife.myReportNew.reports.TimeCard;
import ph.com.applife.myReportNew.reports.maintenance;
import ph.com.applife.myReportNew.reports.technician;

public class Dashboard extends AppCompatActivity implements Updatehelper.OnUpdateNeededListener {
    private FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String,Object> firebaseDefault;
    private final String LATEST_APP_VERSION_KEY = "latest_app_version";
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    Intent profile, changepassword, logout, purch;
    Typeface font;
    Boolean purchased;
    InterstitialAd adview2;
    Session session;
    CardView general,driver,site,time,agent,diser,sales,technician,maintenance,roving;
    String text;
    TextView name;
    //private SharedPreference sharedPreference;
    Activity context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String versionName = BuildConfig.VERSION_NAME;
        getSupportActionBar().setTitle("v"+versionName);

        initNavigationDrawer();




        //Updatehelper.with(this).onUpdateNeeded(this).check();
        //sharedPreference = new SharedPreference();
        general = (CardView)findViewById(R.id.gf);
        driver = (CardView)findViewById(R.id.df);
        site = (CardView)findViewById(R.id.sl);
        agent = (CardView)findViewById(R.id.agent);
        sales = (CardView)findViewById(R.id.sales);
        diser = (CardView)findViewById(R.id.diser);
        time = (CardView)findViewById(R.id.time);
        roving = (CardView)findViewById(R.id.roving);

        technician = (CardView)findViewById(R.id.technician);
        maintenance = (CardView)findViewById(R.id.maintenance);
        //text = sharedPreference.getValue(context);
        session = new Session(this);
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Dashboard.this, GenericForm.class);
                Dashboard.this.startActivity(in);
                Dashboard.this.finish();
            }
        });

        adview2 = new InterstitialAd(Dashboard.this);
        adview2.setAdUnitId(getString(R.string.inter));
        // mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        //session.setPaid(true);

        if (!session.getpaid()) {
            adview2.loadAd(adRequest);
        }
        adview2.setAdListener(new AdListener(){
            public void onAdLoaded(){
                displayInterstitial();
            }
        });

        if (session.getNAME().equals("")) {
            session.setlogin(false);
            logout = new Intent(Dashboard.this,Login.class);
            Dashboard.this.startActivity(logout);
            Dashboard.this.finish();
        }

        firebaseDefault = new HashMap<>();
        firebaseDefault.put(LATEST_APP_VERSION_KEY,getCurrentVersionCode());
        firebaseRemoteConfig.setDefaults(firebaseDefault);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()){
                        firebaseRemoteConfig.activateFetched();
                        checkForUpdate();
                }
                else {
                    Toast.makeText(Dashboard.this, "Something went wrong fetching", Toast.LENGTH_SHORT).show();
                }

            }
        });
        roving.setOnClickListener(new View.OnClickListener() {
            @Override


            public void onClick(View view) {

                if (!session.getpaid()) {
                    selectone();
                } else {
                    Intent in = new Intent(Dashboard.this, RovingForm.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }

        });
        driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!session.getpaid()) {
                    selectone();
                 }
                else {
                    Intent in = new Intent(Dashboard.this, Logistics.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }
        });
        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!session.getpaid()) {
                    selectone();
                }
                else {
                    Intent in = new Intent(Dashboard.this, SiteLocator.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
               }
            }
        });
        agent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!session.getpaid()) {
                        selectone();
                    }
                    else {
                        Intent in = new Intent(Dashboard.this, Agent.class);
                        Dashboard.this.startActivity(in);
                        Dashboard.this.finish();
                   }
                }

        });

        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!session.getpaid()) {
                    selectone();
                }
                else {
                    Intent in = new Intent(Dashboard.this, SalesReport.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }
        });

        diser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!session.getpaid()) {
                    selectone();
                }
                else {
                    Intent in = new Intent(Dashboard.this, Diser.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }
        });

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Dashboard.this, TimeCard.class);
                Dashboard.this.startActivity(in);
                Dashboard.this.finish();
            }
        });

        technician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!session.getpaid()) {
                    selectone();
                }
                else {
                    Intent in = new Intent(Dashboard.this, technician.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }
        });
        maintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!session.getpaid()) {
                    selectone();
                } else {
                    Intent in = new Intent(Dashboard.this, maintenance.class);
                    Dashboard.this.startActivity(in);
                    Dashboard.this.finish();
                }
            }



        });


    }
    private void displayInterstitial(){
        if (adview2.isLoaded()){
            adview2.show();
        }
        else{
           // Toast.makeText(this, "not show", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkForUpdate() {
        int fetchedVersionCode = (int) firebaseRemoteConfig.getDouble(LATEST_APP_VERSION_KEY);
        if(getCurrentVersionCode() < fetchedVersionCode) {

            Snackbar snackbar = Snackbar
                    .make(time, "New version available", Snackbar.LENGTH_LONG)
                    .setAction("Update", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            redirectStore("https://play.google.com/store/apps/details?id=ph.com.applife.myReportNew");
                        }
                    });

            snackbar.show();



        }
        else {
            Snackbar snackbar = Snackbar
                    .make(time, "Your app is updated", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private int getCurrentVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(),0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return  -1;
    }

    // Declaring drawer on the left side of the app
    public void initNavigationDrawer() {
        NavigationView navigationView = (NavigationView)findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id){
                    case R.id.home:
                        //Refresh Home Activity
                        Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.profile:
                        //Go to Profile Activity
                        profile = new Intent(Dashboard.this,Profile.class);
                        Dashboard.this.startActivity(profile);
                        Dashboard.this.finish();
                        break;



                        case R.id.premium:
                            //Refresh Home Activity
                            purch = new Intent(Dashboard.this,Puchase.class);
                            Dashboard.this.startActivity(purch);
                            Dashboard.this.finish();
                            break;


                    case R.id.changepassword:
                        //Go to ChangePassword Activity
                        changepassword = new Intent(Dashboard.this, ChangePassword.class);
                        Dashboard.this.startActivity(changepassword);
                        Dashboard.this.finish();
                        break;

                    case R.id.logout:
                        //Go to Login Activity
                        session.setlogin(false);
                        logout = new Intent(Dashboard.this,Login.class);
                        Dashboard.this.startActivity(logout);
                        Dashboard.this.finish();


                }
                return true;
            }
        });

          View header = navigationView.getHeaderView(0);
        TextView tv_email = (TextView)header.findViewById(R.id.mobile_app);tv_email.setText("myReport Mobile App");

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    private void selectone() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please Purchase the full version.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }


    @Override
    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create();
        dialog.show();
    }
    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



}

