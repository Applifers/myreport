package ph.com.applife.myReportNew;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import ph.com.applife.myReportNew.reports.SiteLocator;

@SuppressWarnings("ALL")
public class SiteLocator_images extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    public Bundle getBundle;
    ImageView img1, img2, img3, img4,img1_gone, img2_gone, img3_gone, img4_gone;
    TextView text1;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD1_IMG = 2;
    private static int RESULT_LOAD2_IMG = 3;
    private static int RESULT_LOAD3_IMG = 4;
    String name1,name2,name3,name4;
    String imageone, imagetwo,imagethree, imagefour;
    String encodedString1,encodedString2,encodedString3,encodedString4;
    Bitmap bitmap1,bitmap2,bitmap3,bitmap4;
    String reportID;
    public static String combo;
    String nameoflocator,position,alias, sendto,emailaddress, date,time,nameofestablishment,address,population,rental,leaseterm,contactperson,subject,mobilenumber,competitors,remarks,gpslatitude,gpslongitude;
    Button rotate1,rotate3;
    final Context context = this;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_locator_images);

        keepInmind();

        //Declare "Gothic" font
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");



        //Checking the Permission Access
        ActivityCompat.requestPermissions(SiteLocator_images.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);

        // Declare Text View
        text1=(TextView)findViewById(R.id.textView17);
        text1.setTypeface(custom_font);
        //testing for randomReportID
        //first random
        char[] chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++)
        {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();

        //second random
        char[] number = "1234567890".toCharArray();
        StringBuilder sb2 = new StringBuilder();
        Random random2 = new Random();
        for (int i = 0; i < 6; i++)
        {
            char c2 = number[random2.nextInt(number.length)];
            sb2.append(c2);
        }
        String random_string1 = sb2.toString();

        //third random
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(c.getTime());

        combo = random_string + "-" + random_string1 + "-" + formattedDate;
        // Receiving variables from Site Locator Activity
        getBundle = null;
        getBundle = this.getIntent().getExtras();
        reportID=combo;
        //noinspection ConstantConditions
        nameoflocator = getBundle.getString("nameoflocator");
        position = getBundle.getString("position");
        alias = getBundle.getString("alias");
        sendto = getBundle.getString("sendto");
        emailaddress = getBundle.getString("emailaddress");
        date = getBundle.getString("date");
        time = getBundle.getString("time");
        nameofestablishment = getBundle.getString("nameofestablishment");
        address = getBundle.getString("address");
        population = getBundle.getString("population");
        rental = getBundle.getString("rental");
        leaseterm = getBundle.getString("leaseterm");
        contactperson = getBundle.getString("contactperson");
        subject = getBundle.getString("subject");
        mobilenumber = getBundle.getString("mobilenumber");
        competitors = getBundle.getString("competitors");
        remarks= getBundle.getString("remarks");
        gpslatitude = getBundle.getString("gpslatitude");
        gpslongitude = getBundle.getString("gpslongitude");

        //Declare Image View
        img1=(ImageView) findViewById(R.id.imageView1);
        img2=(ImageView) findViewById(R.id.imageView2);
        img3=(ImageView) findViewById(R.id.imageView3);
        img4=(ImageView) findViewById(R.id.imageView4);

        img1_gone=(ImageView) findViewById(R.id.imageView7);
        img2_gone=(ImageView) findViewById(R.id.imageView8);
        img3_gone=(ImageView) findViewById(R.id.imageView9);
        img4_gone=(ImageView) findViewById(R.id.imageView10);

        //Declaring Button
        rotate1 = (Button)findViewById(R.id.button14);
        rotate1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (img1.getDrawable() == null) {
                    completeattachment();
                }else if (img1.getDrawable() != null) {
                    img1.setImageBitmap(rotateImage(BitmapFactory.decodeFile(name1), 270));
                }else if (img2.getDrawable() != null) {
                    img2.setImageBitmap(rotateImage(BitmapFactory.decodeFile(name2), 270));
                }
            }
        });

        //Declaring Button
        rotate3 = (Button)findViewById(R.id.button16);
        rotate3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (img1.getDrawable() == null) {
                    completeattachment();
                }else if (img3.getDrawable() != null){
                    img3.setImageBitmap(rotateImage(BitmapFactory.decodeFile(name3), 270));
                }else if (img4.getDrawable() != null){
                    img4.setImageBitmap(rotateImage(BitmapFactory.decodeFile(name4), 270));
                }
            }
        });

        //Declaring Button
        Button button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SiteLocator_images.this, "Thank you for using this app.", Toast.LENGTH_LONG).show();
                moveTaskToBack(true);
                Intent intent = new Intent(SiteLocator_images.this, Dashboard.class);
                startActivity(intent);
                SiteLocator_images.this.finish();
            }
        });
    }

    // Rotate Image
    public Bitmap rotateImage(Bitmap src, float degree) {
        // create new matrix object
        Matrix matrix = new Matrix();
        // setup rotation degree
        matrix.postRotate(degree);
        // return new bitmap rotated using matrix
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    //Checking the image is visible or not
    public void image1(View view) {
        if (img1_gone.getVisibility() == View.VISIBLE) {
            img1_gone.setVisibility(View.INVISIBLE);
        }
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }
    //Checking the image is visible or not
    public void image2(View view) {
        if (img2_gone.getVisibility() == View.VISIBLE) {
            img2_gone.setVisibility(View.INVISIBLE);
        }
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD1_IMG);
    }
    //Checking the image is visible or not
    public void image3(View view) {
        if (img3_gone.getVisibility() == View.VISIBLE) {
            img3_gone.setVisibility(View.INVISIBLE);
        }
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD2_IMG);
    }
    //Checking the image is visible or not
    public void image4(View view) {
        if (img4_gone.getVisibility() == View.VISIBLE) {
            img4_gone.setVisibility(View.INVISIBLE);
        }
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD3_IMG);
    }
    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                //noinspection ConstantConditions
                @SuppressWarnings("ConstantConditions") Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                //noinspection ConstantConditions
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                name1 = cursor.getString(columnIndex);
                cursor.close();

                File img = new File(name1);
                long length = img.length() /(1024 * 3);

                if(length > 2000){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Error!");
                    builder.setIcon( android.R.drawable.ic_dialog_alert);
                    builder.setMessage("The image you pick is more than the limit set, please select another photo.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK",null);
                    builder.show();
                } else {
                    img1.setImageBitmap(BitmapFactory.decodeFile(name1));
                }
            }// When an Image is picked
            else if (requestCode == RESULT_LOAD1_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                //noinspection ConstantConditions
                @SuppressWarnings("ConstantConditions") Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                //noinspection ConstantConditions
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                name2= cursor.getString(columnIndex);
                cursor.close();

                File img = new File(name2);
                long length = img.length() /(1024 * 3);

                if(length > 2000){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Error!");
                    builder.setIcon( android.R.drawable.ic_dialog_alert);
                    builder.setMessage("The image you pick is more than the limit set, please select another photo.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK",null);
                    builder.show();
                } else {
                    // Set the Image in ImageView after decoding the String
                    img2.setImageBitmap(BitmapFactory.decodeFile(name2));
                }
            }// When an Image is picked
            else if (requestCode == RESULT_LOAD2_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                //noinspection ConstantConditions
                @SuppressWarnings("ConstantConditions") Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                //noinspection ConstantConditions
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                name3= cursor.getString(columnIndex);
                cursor.close();

                File img = new File(name3);
                long length = img.length() /(1024 * 3);

                if(length > 2000){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Error!");
                    builder.setIcon( android.R.drawable.ic_dialog_alert);
                    builder.setMessage("The image you pick is more than the limit set, please select another photo.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK",null);
                    builder.show();
                } else {
                    // Set the Image in ImageView after decoding the String
                    img3.setImageBitmap(BitmapFactory.decodeFile(name3));
                }
            } // When an Image is picked
            else if (requestCode == RESULT_LOAD3_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                //noinspection ConstantConditions
                @SuppressWarnings("ConstantConditions") Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                //noinspection ConstantConditions
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                name4= cursor.getString(columnIndex);
                cursor.close();

                File img = new File(name4);
                long length = img.length() /(1024 * 3);

                if(length > 2000){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
                    builder.setTitle("Error!");
                    builder.setIcon( android.R.drawable.ic_dialog_alert);
                    builder.setMessage("The image you pick is more than the limit set, please select another photo.");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK",null);
                    builder.show();
                } else {
                    // Set the Image in ImageView after decoding the String
                    img4.setImageBitmap(BitmapFactory.decodeFile(name4));
                }
            }else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
           somethingwentwrong();
        }
    }

    public void btnSend(View view){
        //check if my internet connection
        isInternetOn();

        if (img1.getDrawable() == null) {
            //Check if empty
            completeattachment();
        } else if (img2.getDrawable() == null) {
            //Check if empty
           completeattachment();
        }  else if (img3.getDrawable() == null) {
            //Check if empty
           completeattachment();
        }  else if (img4.getDrawable() == null) {
            //Check if empty
            completeattachment();
        }
        else {
    // first image
            BitmapFactory.Options options = null;
            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            bitmap1 = BitmapFactory.decodeFile(name1, options);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap1.compress(Bitmap.CompressFormat.PNG, 50, stream);

            byte[] byte_arr1 = stream.toByteArray();

            // Encode Image to String
            encodedString1 = Base64.encodeToString(byte_arr1, 0);
            imageone= encodedString1;

    //second image
            BitmapFactory.Options options1 = null;
            options1 = new BitmapFactory.Options();
            options1.inSampleSize = 2;

            bitmap2 = BitmapFactory.decodeFile(name2, options1);
            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap2.compress(Bitmap.CompressFormat.PNG, 50, stream1);
            byte[] byte_arr2 = stream1.toByteArray();
            // Encode Image to String
            encodedString2 = Base64.encodeToString(byte_arr2, 0);
            imagetwo= encodedString2;

      // third image
            BitmapFactory.Options options2 = null;
            options2 = new BitmapFactory.Options();
            options2.inSampleSize = 2;

            bitmap3 = BitmapFactory.decodeFile(name3, options2);
            ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap3.compress(Bitmap.CompressFormat.PNG, 50, stream2);
            byte[] byte_arr3 = stream2.toByteArray();
            // Encode Image to String
            encodedString3 = Base64.encodeToString(byte_arr3, 0);
            imagethree= encodedString3;

      //fourth image
            BitmapFactory.Options options3 = null;
            options3 = new BitmapFactory.Options();
            options3.inSampleSize = 2;

            bitmap4 = BitmapFactory.decodeFile(name4, options3);
            ByteArrayOutputStream stream3 = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap4.compress(Bitmap.CompressFormat.PNG, 50, stream3);
            byte[] byte_arr4 = stream3.toByteArray();
            // Encode Image to String
            encodedString4 = Base64.encodeToString(byte_arr4, 0);
            imagefour= encodedString4;

            // Calling the function of AsyncReport for registering the data from the Site Locator form 1 and form 2
            new AsyncReport().execute(nameoflocator, position,alias,sendto,emailaddress, date, time, nameofestablishment, address, population, rental, leaseterm, contactperson,subject, mobilenumber, competitors, remarks, gpslatitude, gpslongitude, imageone, imagetwo, imagethree, imagefour, reportID);
        }
    }

    private class AsyncReport extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(SiteLocator_images.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/sitelocatorcode.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("nameoflocator", params[0])
                        .appendQueryParameter("position", params[1])
                        .appendQueryParameter("alias", params[2])
                        .appendQueryParameter("sendto", params[3])
                        .appendQueryParameter("emailaddress", params[4])
                        .appendQueryParameter("date", params[5])
                        .appendQueryParameter("time", params[6])
                        .appendQueryParameter("nameofestablishment", params[7])
                        .appendQueryParameter("address", params[8])
                        .appendQueryParameter("population", params[9])
                        .appendQueryParameter("rental", params[10])
                        .appendQueryParameter("leaseterm", params[11])
                        .appendQueryParameter("contactperson", params[12])
                        .appendQueryParameter("subject", params[13])
                        .appendQueryParameter("mobilenumber", params[14])
                        .appendQueryParameter("competitors", params[15])
                        .appendQueryParameter("remarks", params[16])
                        .appendQueryParameter("gpslatitude", params[17])
                        .appendQueryParameter("gpslongitude", params[18])
                            .appendQueryParameter("imageone", params[19])
                            .appendQueryParameter("imagetwo", params[20])
                        .appendQueryParameter("imagethree", params[21])
                        .appendQueryParameter("imagefour", params[22])
                        .appendQueryParameter("reportID", params[23]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return (result.toString());
                } else {
                    return ("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            pdLoading.dismiss();

            if (result.equalsIgnoreCase("true")) {
                // Calling success function
                success();
            } else if (result.equalsIgnoreCase("false")) {
                //Calling somethingwentwrong function
                somethingwentwrong();
            }
        }
    }

    private void keepInmind() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.info_title);
            builder.setMessage(R.string.info_description);
            builder.setPositiveButton(R.string.ok, null);
            builder.show();
        }

    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Intent intent = new Intent(SiteLocator_images.this, SiteLocator.class);
                        startActivity(intent);
                        SiteLocator_images.this .finish();
                    }
                });
        builder.show();
    }

    private void somethingwentwrong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Something went wrong. Please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void completeattachment() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the attachment");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.messageoferror);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    //Check if Internet Connection is connected if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
                // if connected with internet
                return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
                error();
                return false;
        }
        return false;
    }

    private void connectingtoserver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Connecting to server, please wait..");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press back to Dashboard.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
}
