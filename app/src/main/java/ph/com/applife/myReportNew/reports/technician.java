package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import ph.com.applife.myReportNew.ApplifeUtils;
import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.HttpRequest;
import ph.com.applife.myReportNew.MainActivity;
import ph.com.applife.myReportNew.ParseContent;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Session;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
public class technician extends Activity  implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    Button send,cancel;
    EditText subj,oper,find,reso,reco,rema;
    ParseContent parseContent;
    ProgressDialog progressDialog;
    Bitmap bitmap;
    Session session;
    Uri actualUri;
    private LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    static Location mLastLocation;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    ImageView imageView;
    private static final String IMAGE_DIRECTORY = "/myreportimage";
    private boolean haveimage,timein,imahe;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 3, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1, CAMERA = 2;
     String HttpUrl1 = "http://applife.com.ph/ireport/technician.php";
    private String area;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.technicians);
        parseContent = new ParseContent(this);
        session = new Session(this);
        send = (Button)findViewById(R.id.button5);
        cancel = (Button)findViewById(R.id.button3);
        subj = (EditText)findViewById(R.id.subj);
        oper = (EditText)findViewById(R.id.operator);
        find = (EditText)findViewById(R.id.finds);
        reso = (EditText)findViewById(R.id.reso);
        reco = (EditText)findViewById(R.id.recom);
        rema = (EditText)findViewById(R.id.remarks);
        imageView = (ImageView)findViewById(R.id.imageView);
        requestStoragePermission();
        startLocationUpdates();


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startLocationUpdates();
                if (subj.getText().toString().length() == 0) {
                    subj.setError("Please enter Subject!");
                    subj.requestFocus();
                    return ;
                } else if (oper.getText().toString().length() == 0) {
                    oper.setError("Please enter Operator name!");
                    oper.requestFocus();
                    return ;
                }  else if (find.getText().toString().length() == 0) {
                    find.setError("Please enter findins!");
                    find.requestFocus();
                    return ;
                } else if (reso.getText().toString().length() == 0) {
                    reso.setError("Please enter Resolution!");
                    reso.requestFocus();
                    return ;
                }else if (reco.getText().toString().length() == 0 ) {
                    reco.setError("Please enter Recommendation!");
                    reco.requestFocus();
                    return ;
                }else if (rema.getText().toString().length() == 0 ) {
                    rema.setError("Please enter Remarks!");
                    rema.requestFocus();
                    return ;
                }

                if (!haveimage) {
                    Toast.makeText(technician.this, "Please put image for reference", Toast.LENGTH_SHORT).show();
                   
                    return ;
                }
                else {
                    ImageUploadToServerFunction();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(technician.this, Dashboard.class);
                startActivity(intent);
                technician.this.finish();
            }
        });
    }

    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());


    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)  && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,}, 6666);

    }
    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, CAMERA);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code READ_REQUEST_CODE.
        // If the request code seen here doesn't match, it's the response to some other intent,
        // and the below code shouldn't run at all.
        super.onActivityResult(requestCode, resultCode, resultData);


        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && resultCode == Activity.RESULT_OK ) {

            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.  Pull that uri using "resultData.getData()"
            //actualUri = resultData.getData();
            if (resultData != null) {



                actualUri = resultData.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    //chosen.setImageBitmap(bitmap);
                    //chosen.invalidate();
                    //rotatebutton.setEnabled(true);
                    //haveimage = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                haveimage = false;
            }
        }

        if (requestCode == CAMERA && resultCode == RESULT_OK) {

            haveimage = true;
            bitmap = (Bitmap) resultData.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 1000, 1000, true);
            imageView.setImageBitmap(bitmap);

        }
        else {
            Toast.makeText(technician.this, "canceled", Toast.LENGTH_SHORT).show();
        }


    }


    private void ImageUploadToServerFunction() {

        if (!ApplifeUtils.isNetworkAvailable(technician.this)) {
            Toast.makeText(technician.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        ByteArrayOutputStream byteArrayOutputStreamObject;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        //rotated = Bitmap.createScaledBitmap(rotated, 600, 600, true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(technician.this, "Image is Uploading", "Please Wait", false, false);
            }


            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                Log.d("newwwss", string1);
                onTaskCompleted(string1,1);


            }
            private void onTaskCompleted(String response,int task) {
                Log.d("responsejson", response.toString());
                progressDialog.dismiss();  //will remove progress dialog
                switch (task) {
                    case 1:
                        if (parseContent.isSuccess(response)) {
                            Toast.makeText(technician.this, "success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(technician.this, technician.class);
                            startActivity(intent);
                            technician.this.finish();

                        } else {
                            Toast.makeText(technician.this, "Error Please Try again", Toast.LENGTH_SHORT).show();
                        }
                }
            }


            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClass imageProcessClass = new ImageProcessClass();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("user", session.getNAME());
                HashMapParams.put("subject", subj.getText().toString());
                HashMapParams.put("fullname", session.getNAME());
                HashMapParams.put("operator",oper.getText().toString());
                HashMapParams.put("findings", find.getText().toString());
                HashMapParams.put("reso", reso.getText().toString());
                HashMapParams.put("loc", area);
                HashMapParams.put("recommend", reco.getText().toString());
                HashMapParams.put("remarks", rema.getText().toString());
                HashMapParams.put("sendto", session.getSENDTO());
                HashMapParams.put("image_path", ConvertImage);
                HashMapParams.put("def",getSaltString());
                String FinalData = imageProcessClass.ImageHttpRequest(HttpUrl1, HashMapParams);

                return FinalData;

            }


        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();



    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }
    private void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(technician.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;
                }
            }

        });
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

            } else {

                Log.i("Current Location", "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        lat = Double.toString(location.getLatitude());
//        longi= Double.toString(location.getLongitude());
        //Toast.makeText(getApplicationContext(), lat + " , " +longi,Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, mss, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps


        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses  = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                area = addresses.get(0).getAddressLine(0);
                //Toast.makeText(getApplicationContext(), area, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("tag", e.getMessage());
        }

    }

    private class ImageProcessClass {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();
                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");
                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {


                stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }


    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(technician.this, Dashboard.class);
        startActivity(intent);
        technician.this.finish();

    }
}