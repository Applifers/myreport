package ph.com.applife.myReportNew;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteHelper extends SQLiteOpenHelper {

    // database version
    private static final int database_VERSION = 1;
    // database name
    private static final String DATABASE_NAME = "ItemDB";
    public static final String table_ITEM = "iteminventory";
    public static  final String ITEM_ID = "id";
    public static final String ITEM_ITEMCODE = "itemcode";
    public static final String ITEM_PRODUCT = "product";
    public static final String ITEM_BEGINNINGINVENTORY = "beginninginventory";
    public static final String ITEM_EXPIRYDATE = "expirydate";
    public static final String ITEM_SOLIDSTOCKS = "solidstocks";
    public static final String ITEM_STOCKSFORPULLOUT = "stocksforpullout";
    public static final String ITEM_EXPIRYDATEPULLOUT = "expirydatepullout";
    public static final String ITEM_DELIVERYITEMS = "deliveryitems";
    public static final String ITEM_ENDINGINVENTORY = "endinginventory";
    public static final String ITEM_WAREHOUSE = "warehouse";
    public static final String ITEM_RDUSTOCKS = "rdustocks";
    public static final String ITEM_RATBITE = "ratbite";
    private HashMap hp;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME , null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table iteminventory " +
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT, itemcode text, product text, beginninginventory text, expirydate text,solidstocks text, stocksforpullout text, expirydatepullout text, deliveryitems text, endinginventory text, warehouse text, rdustocks text, ratbite text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS" + table_ITEM);
        onCreate(db);
    }

    // Function insertItem
    public boolean insertItem (String itemcode, String product, String beginninginventory, String expirydate,String solidstocks, String stocksforpullout,
                               String expirydatepullout, String deliveryitems, String endinginventory, String warehouse, String rdustocks, String ratbite ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ITEM_ITEMCODE, itemcode);
        contentValues.put(ITEM_PRODUCT, product);
        contentValues.put(ITEM_BEGINNINGINVENTORY, beginninginventory);
        contentValues.put(ITEM_EXPIRYDATE , expirydate);
        contentValues.put(ITEM_SOLIDSTOCKS, solidstocks);
        contentValues.put(ITEM_STOCKSFORPULLOUT, stocksforpullout);
        contentValues.put(ITEM_EXPIRYDATEPULLOUT , expirydatepullout);
        contentValues.put(ITEM_DELIVERYITEMS, deliveryitems);
        contentValues.put(ITEM_ENDINGINVENTORY , endinginventory);
        contentValues.put(ITEM_WAREHOUSE, warehouse);
        contentValues.put(ITEM_RDUSTOCKS, rdustocks);
        contentValues.put(ITEM_RATBITE, ratbite);
        db.insert(table_ITEM , null, contentValues);
        return true;
    }

    // Function getData
    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from iteminventory where id=" + id +" ", null );
        return res;
    }

    //Fucntion deleteAll
    public void deleteall(){
        SQLiteDatabase db = this.getWritableDatabase();
        String delete = "DELETE  FROM iteminventory";
        db.rawQuery(delete, null);
    }

    // Function numberOfRows
    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, table_ITEM);
        return numRows;
    }

    //Function updateItem
    public boolean updateItem (Integer id, String itemcode, String product, String beginninginventory, String expirydate,String solidstocks, String stocksforpullout,
                               String expirydatepullout, String deliveryitems, String endinginventory, String warehouse, String rdustocks, String ratbite ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ITEM_ITEMCODE, itemcode);
        contentValues.put(ITEM_PRODUCT, product);
        contentValues.put(ITEM_BEGINNINGINVENTORY, beginninginventory);
        contentValues.put(ITEM_EXPIRYDATE , expirydate);
        contentValues.put(ITEM_SOLIDSTOCKS, solidstocks);
        contentValues.put(ITEM_STOCKSFORPULLOUT, stocksforpullout);
        contentValues.put(ITEM_EXPIRYDATEPULLOUT , expirydatepullout);
        contentValues.put(ITEM_DELIVERYITEMS, deliveryitems);
        contentValues.put(ITEM_ENDINGINVENTORY , endinginventory);
        contentValues.put(ITEM_WAREHOUSE, warehouse);
        contentValues.put(ITEM_RDUSTOCKS, rdustocks);
        contentValues.put(ITEM_RATBITE, ratbite);
        db.update(table_ITEM, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    // Function deleteItem
    public Integer deleteItem (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(table_ITEM,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    // Function getAllItem
    public ArrayList<String> getAllItem() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select itemcode from " + table_ITEM , null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(table_ITEM)));
            res.moveToNext();
        }
        return array_list;
    }
}