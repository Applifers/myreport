package ph.com.applife.myReportNew;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class forotP extends Activity {

    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int READ_TIMEOUT = 15000;
   private EditText emailadd;
    String date;
    String getDate;
    String code;
    String status;
    public static String combo;
    public static String email;
    public static String fromdate;
    public static String fromcode;
    public static String fromstatus;

    public final String stat="1";

    private static final String TAG_email = "email";
    private static final String TAG_date="date";
    private static final String TAG_code="code";
    private static final String TAG_status="status";

    SessionManager manager;
    private SharedPreference sharedPreference;
    Activity context = this;
    Typeface font;
    private String text;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forot_p);
        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        manager = new SessionManager();
        sharedPreference = new SharedPreference();

        //testing for randomReportID
        //first random
        char[] chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 4; i++)
        {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();

        //second random
        char[] number = "1234567890".toCharArray();
        StringBuilder sb2 = new StringBuilder();
        Random random2 = new Random();
        for (int i = 0; i < 3; i++)
        {
            char c2 = number[random2.nextInt(number.length)];
            sb2.append(c2);
        }
        String random_string1 = sb2.toString();



        combo = random_string + random_string1;

        /* Declare Text View */
        emailadd= (EditText) findViewById(R.id.username);
        emailadd.setTypeface(custom_font);
        //Declaring Date (Year-Month-Date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate=(dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate=(dateFormat.format(new Date()));

    }
    public void sendRequest(View Arg0) {
        //check if my internet connection
        isInternetOn();
        email = emailadd.getText().toString();
        code = combo;
        status = stat;
        date = getDate;
        // Messages if empty
        if (email.equals("")) {
            //Check if empty
            completetheform();
        } else {
            // Calling the function of AsyncRegister for registering the data from the Register Form
            new AsyncforotP().execute(email, date, code, status);
        }
    }
    private class AsyncforotP extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(forotP.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/resetcode.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);
                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("email", params[0])
                        .appendQueryParameter("date", params[1])
                        .appendQueryParameter("code", params[2])
                        .appendQueryParameter("status", params[3]);
                String query = builder.build().getEncodedQuery();
                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }
            try {
                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return (result.toString());
                } else {
                    return ("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();

            if (result.equalsIgnoreCase("nyay!")) {
                //Calling eaddexist function
                eaddexist();
            } else if (result.equalsIgnoreCase("false")) {
                // Calling somethingwentwrong function
                somethingwentwrong();
            } else {
                // Calling success function
                success();
            }
        }
    }
        private void success() {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.title);
            builder.setIcon( android.R.drawable.ic_menu_save);
            builder.setMessage("Please Check your Email for the Reset Code!");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            Intent intent = new Intent(forotP.this, ResetCode.class);
                            startActivity(intent);
                            forotP.this.finish();
                        }
                    });
            builder.show();
        }

        private void somethingwentwrong() {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
            builder.setTitle(R.string.title);
            builder.setIcon( android.R.drawable.stat_notify_error);
            builder.setMessage("Something went wrong. Please try again.");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",null);
            builder.show();
        }
    private void welcome() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_dialog_info);
        builder.setMessage("Please Check your email for Reset Code, Thank You!");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    private void eaddexist() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Invalid Email!");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please check your Internet connection");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    //Check if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }
}