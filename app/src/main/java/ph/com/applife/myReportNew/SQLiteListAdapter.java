package ph.com.applife.myReportNew;

/**
 * Created by mina on 11/17/2016.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

@SuppressWarnings("ALL")
public class SQLiteListAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> ID;
    ArrayList<String> Product;
    ArrayList<String> BeginningInventory;
    ArrayList<String> Solidstocks;
    ArrayList<String> ExpireDate ;
    ArrayList<String> Stockspullout;
    ArrayList<String> Stocksexpired;
    ArrayList<String> Deliveryitem;
    ArrayList<String> Endinginventory;
    ArrayList<String> Warehouse;
    ArrayList<String> Rdustocks;
    ArrayList<String> Ratbite;


    public SQLiteListAdapter(
            Context context2,
            ArrayList<String> id,
            ArrayList<String> product,
            ArrayList<String> beginningInventory,
            ArrayList<String> solidstocks,
            ArrayList<String> expireDate,
            ArrayList<String> stockspullout,
            ArrayList<String> stocksexpired,
            ArrayList<String> deliveryitem,
            ArrayList<String> endinginventory,
            ArrayList<String> warehouse,
            ArrayList<String> rdustocks,
            ArrayList<String> ratbite
    )

    {

        this.context = context2;
        this.ID = id;
        this.Product = product;
        this.BeginningInventory = beginningInventory;
        this.Solidstocks= solidstocks;
        this.ExpireDate = expireDate;
        this.Stockspullout = stockspullout;
        this.Stocksexpired = stocksexpired;
        this.Deliveryitem = deliveryitem;
        this.Endinginventory = endinginventory;
        this.Warehouse = warehouse;
        this.Rdustocks = rdustocks;
        this.Ratbite = ratbite;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return ID.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @SuppressWarnings("ConstantConditions")
    public View getView(int position, View child, ViewGroup parent) {

        Holder holder;

        LayoutInflater layoutInflater;

        if (child == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //noinspection ConstantConditions
            child = layoutInflater.inflate(R.layout.row_layout, null);

            holder = new Holder();

            holder.textviewid = (TextView) child.findViewById(R.id.itemcode);
            holder.textviewproduct = (TextView) child.findViewById(R.id.product);
            holder.textviewbeginninginventory = (TextView) child.findViewById(R.id.beginninginventory);
            holder.textviewsolidstocks = (TextView) child.findViewById(R.id.solidstocks);
            holder.textviewexpirydate = (TextView) child.findViewById(R.id.expirydate);
            holder.textviewstocksforpullout = (TextView) child.findViewById(R.id.stocksforpullout);
            holder.textviewexpirydateforpullout = (TextView) child.findViewById(R.id.expirydateforpullout);
            holder.textviewdeliveryitems = (TextView) child.findViewById(R.id.deliveryitems);
            holder.textviewendinginventory = (TextView) child.findViewById(R.id.endinginventory);
            holder.textviewwarehouse = (TextView) child.findViewById(R.id.warehouse);
            holder.textviewrdustocks = (TextView) child.findViewById(R.id.rdustocks);
            holder.textviewratbite = (TextView) child.findViewById(R.id.ratbite);

            child.setTag(holder);

        } else {

            holder = (Holder) child.getTag();
        }
        holder.textviewid.setText(ID.get(position));
        holder.textviewproduct.setText(Product.get(position));
        holder.textviewbeginninginventory.setText(BeginningInventory.get(position));
        holder.textviewsolidstocks.setText(Solidstocks.get(position));
        holder.textviewexpirydate.setText(ExpireDate.get(position));
        holder.textviewstocksforpullout.setText(Stockspullout.get(position));
        holder.textviewexpirydateforpullout.setText(Stocksexpired.get(position));
        holder.textviewdeliveryitems.setText(Deliveryitem.get(position));
        holder.textviewendinginventory.setText(Endinginventory.get(position));
        holder.textviewwarehouse.setText(Warehouse.get(position));
        holder.textviewrdustocks.setText(Rdustocks.get(position));
        holder.textviewratbite.setText(Ratbite.get(position));

        return child;
    }

    public class Holder {
        TextView textviewid;
        TextView textviewproduct;
        TextView textviewbeginninginventory;
        TextView textviewsolidstocks;
        TextView textviewexpirydate;
        TextView textviewstocksforpullout;
        TextView textviewexpirydateforpullout;
        TextView textviewdeliveryitems;
        TextView textviewendinginventory;
        TextView textviewwarehouse;
        TextView textviewrdustocks;
        TextView textviewratbite;
    }
}