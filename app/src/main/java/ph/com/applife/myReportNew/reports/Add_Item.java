package ph.com.applife.myReportNew.reports;
import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

import ph.com.applife.myReportNew.MainActivity;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.SQLiteHelper;
import ph.com.applife.myReportNew.SessionManager;
import ph.com.applife.myReportNew.SharedPreference;

public class Add_Item extends Activity {


    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;

    private TextView text1,text2;
    TextView name,iden;
    private EditText itempro,begin, expire, stocks, stockspull, expirestock, deli, end, ware, rdu, bite;
    DatePickerDialog datePickerDialog;
    String format;
    Calendar calendar;
    private int CalendarHour, CalendarMinute;
    TimePickerDialog timepickerdialog;

    String id,nameofproduct,beginningInventory,expireDate, solidStocks, stocksforpullout, stockexpiredate, deliveryitem, endingInventory, warehouseStocks, rduStocks, ratBite;

    // uicontrols
    Spinner product;
    //class members
    String productName[] = {"MIC-BLACK FOREST", "MIC-BUKO PANDAN", "MIC-COOKIES&CREAM", "MIC-CHOCO TRUFFLES", "MIC-CHOCOLATE", "MIC-MANGO", "MIC-STRAWBERRY", "MIC-UBE", "MIC-VANILLA", "MIC-DOUBLE DUTCH", "MIC-CHEESE",
            "STARBLENDS MOCHA", "STARBLENDS CAPPUCCINO", "STARBLENDS WHITE CHOCOLATE", "STARBLENDS DOUBLE DUTCH", "STARBLENDS CHOCO TRUFFLES", "SSK-BLACK FOREST", "SSK-BUKO PANDAN", "SSK-COOKIES&CREAM", "SSK-ROCKY ROAD",
            "SSK-STRAWBERRY", "SSK-TARO UBE", "MI-LEMON ICE TEA", "MI-RED ICE TEA", "BLACK GULAMAN", "PALAMIG MELON", "PALAMIG CHOCOLATE", "PALAMIG MANGO", "PALAMIG STRAWBERRY", "WHIPPING CREAM", "HOTCAKE PREMIX",
            "MINI DONUT PREMIX","WAFFLE ORIGINAL PREMIX", "CHICKEN JOYFUL", "CHOCOLATE SYRUP", "CARAMEL SYRUP", "STRAWBERRY SYRUP", "MCF-3IN1 COFFEE 20gX36s", "MICAFE CHOCO X 20gX36s", "MICAFE 3IN1 COFFEE MIX kl",
            "MICAFE HOT CHOCO MIX 1kl", "MICAFE CAPPUCCINO MIX kl", "MCF-CARAMEL COFFEE MIX kl"};
    ArrayAdapter<String> adapterUserType;


    SessionManager manager;
    private SharedPreference sharedPreference;
    private String text;
    Activity context = this;
    Typeface font;
    SQLiteHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__item);

        //Declare "Gothic" font
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        db = new SQLiteHelper(getApplicationContext());

        manager = new SessionManager();
        sharedPreference = new SharedPreference();

        //Checking the Permission Access
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //Declare Text View
        text1=(TextView)findViewById(R.id.textView29);
        text1.setTypeface(custom_font);
        iden = (TextView) findViewById(R.id.textView28);
        iden.setTypeface(custom_font);
        iden.setText(Diser.combo);

        //Declare Edit Text
        itempro = (EditText) findViewById(R.id.editText50);
        itempro.setTypeface(custom_font);
        begin = (EditText) findViewById(R.id.editText34);
        begin.setTypeface(custom_font);
        stocks = (EditText) findViewById(R.id.editText39);
        stocks.setTypeface(custom_font);
        stockspull = (EditText) findViewById(R.id.editText40);
        stockspull.setTypeface(custom_font);
        deli = (EditText) findViewById(R.id.editText42);
        deli.setTypeface(custom_font);
        end = (EditText) findViewById(R.id.editText43);
        end.setTypeface(custom_font);
        ware = (EditText) findViewById(R.id.editText45);
        ware.setTypeface(custom_font);
        rdu = (EditText) findViewById(R.id.editText46);
        rdu.setTypeface(custom_font);
        bite = (EditText) findViewById(R.id.editText47);
        bite.setTypeface(custom_font);

        // Picking up Date (YEAR-MONTH-DAY)
        expire = (EditText) findViewById(R.id.editText37);
        expire.setTypeface(custom_font);
        expire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(Add_Item.this,new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        expire.setText(year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        // Picking up Date (YEAR-MONTH-DAY)
        expirestock = (EditText) findViewById(R.id.editText41);
        expirestock.setTypeface(custom_font);
        expirestock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                datePickerDialog = new DatePickerDialog(Add_Item.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        expirestock.setText(year + "-"
                                + (monthOfYear + 1) + "-" + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        //Declare Button
        Button button1 = (Button) findViewById(R.id.button11);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itempro.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (begin.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (expire.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (stocks.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (stockspull.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (expirestock.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (deli.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (end.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (ware.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (rdu.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (bite.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else {

                    // Converting Edit Text and Text View to String
                    id = Diser.combo;
                    nameofproduct = itempro.getText().toString();
                    beginningInventory = begin.getText().toString();
                    expireDate = expire.getText().toString();
                    solidStocks = stocks.getText().toString();
                    stocksforpullout = stockspull.getText().toString();
                    stockexpiredate = expirestock.getText().toString();
                    deliveryitem = deli.getText().toString();
                    endingInventory = end.getText().toString();
                    warehouseStocks = ware.getText().toString();
                    rduStocks = rdu.getText().toString();
                    ratBite = bite.getText().toString();

                    //Inserting values in the local database
                    db.insertItem (id,nameofproduct,beginningInventory, expireDate,solidStocks,stocksforpullout, stockexpiredate,deliveryitem,endingInventory,warehouseStocks, rduStocks, ratBite);
                    thankyou();

                }
            }
        });
    }
    //Dropdown setting
    public <ViewGroup> void spinnerType(){
        product= (Spinner) findViewById(R.id.spinner9);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, productName)
        {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                font = Typeface.createFromAsset(getAssets(),"font/Gothic.ttf");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(Color.WHITE);
                v.setTextSize(20);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(Color.BLACK);
                v.setTextSize(20);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        product.setAdapter(adapter1);
    }

    private void thankyou() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Inventory has been successfully saved in Database SQLite, Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Intent intent = new Intent(Add_Item.this, MainActivity.class);
                        startActivity(intent);
                        Add_Item.this.finish();
                    }
                });
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press next to complete the transaction.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

}

