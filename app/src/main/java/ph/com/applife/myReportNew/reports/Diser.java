package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.GPSTracker;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Session;

@SuppressWarnings("ALL")
public class Diser extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    // GPSTracker class
    GPSTracker gps;
    // declaration for numbers
    Double latitude = 0.0;
    Double longitude = 0.0;
    Session session;

    private TextView name,iden,notes, pos,send,email,alias_,text1,text2,text3,text4,text5;
    private EditText  visor, add, mob, branch,contact,sub;
    String getDate, getTime;
    String nameofbranch,position,alias,sendto,emailaddress, nameofdiser, supervisor,contactperson,subject, address, mobilenumber, date, time, remarks, gpslatitude, gpslongitude;
    String id;
    public static String combo,fullname;
    Activity context = this;
    Bundle bundle;
    Typeface font;



    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diser);

        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        session = new Session(this);

        //Checking the Permission Access
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //Declare Text View
        text1=(TextView)findViewById(R.id.textView7);
        text1.setTypeface(custom_font);
        //Declare Text View
        text2=(TextView)findViewById(R.id.textView19);
        text2.setTypeface(custom_font);
        //Declare Text View
        text3= (TextView)findViewById(R.id.textView56);
        text3.setTypeface(custom_font);
        //Declare Text View
        text4=(TextView)findViewById(R.id.textView53);
        text4.setTypeface(custom_font);
        //Declare Text View
        text5=(TextView)findViewById(R.id.textView47);
        text5.setTypeface(custom_font);
        //Declare Text View
        name = (TextView) findViewById(R.id.textView24);
        name.setTypeface(custom_font);
        //Declare Text View
        pos= (TextView)findViewById(R.id.textView57);
        pos.setTypeface(custom_font);
        //Declare Text View
        email=(TextView)findViewById(R.id.textView60);
        email.setTypeface(custom_font);
        //Declare Text View
        send=(TextView)findViewById(R.id.textView59);
        send.setTypeface(custom_font);
        //Declare Text View
        alias_=(TextView)findViewById(R.id.textView64);
        alias_.setTypeface(custom_font);

        //Checking if the variable if empty or not and use other variable and vice versa
            email.setText(session.getEMAIL());
        //Checking if the variable if empty or not and use other variable and vice versa
            send.setText(session.getSENDTO());
            alias_.setText(session.getAlias());
        //Retrieve a value from SharedPreference
        name.setText(session.getNAME());
        //first random
        char[] chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++)
        {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();

        //second random
        char[] number = "1234567890".toCharArray();
        StringBuilder sb2 = new StringBuilder();
        Random random2 = new Random();
        for (int i = 0; i < 6; i++)
        {
            char c2 = number[random2.nextInt(number.length)];
            sb2.append(c2);
        }
        String random_string1 = sb2.toString();

        //third random
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = df.format(c.getTime());

        combo = random_string + "-" + random_string1 + "-" + formattedDate;

        //Declaring Edit View
        iden = (TextView) findViewById(R.id.textView11);
        iden.setTypeface(custom_font);
        iden.setText(combo);
        //Declaring Edit View
        branch = (EditText) findViewById(R.id.editText2);
        branch.setTypeface(custom_font);
        //Declaring Edit View
        visor = (EditText) findViewById(R.id.editText4);
        visor.setTypeface(custom_font);
        //Declaring Edit View
        add = (EditText) findViewById(R.id.editText31);
        add.setTypeface(custom_font);
        //Declaring Edit View
        mob = (EditText) findViewById(R.id.editText7);
        mob.setTypeface(custom_font);
        //Declaring Edit View
        notes = (EditText) findViewById(R.id.editText8);
        notes.setTypeface(custom_font);
        //Declaring Edit View
        contact = (EditText) findViewById(R.id.operator);
        contact.setTypeface(custom_font);
        //Declaring Edit View
        sub = (EditText) findViewById(R.id.finds);
        sub.setTypeface(custom_font);

        //Declaring Date (Year-Month-Date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate= (dateFormat.format(new Date())); // it will show 16/07/2013
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate= (dateFormat.format(new Date())); // it will show 16 Jul 2013

        //Declaring Date (Hour-Minute)
        Date d=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        getTime= (currentDateTimeString);

        //Declaring Button
        Button button1 = (Button) findViewById(R.id.button10);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if my internet connection
                isInternetOn();
                if (add.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (mob.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (mob.getText().toString().length() > 11 ) {
                    //Check if empty
                    digitnumber();
                }else if (contact.getText().toString().length() == 0 ) {
                    //Check if empty
                    digitnumber();
                }else if (sub.getText().toString().length() == 0 ) {
                    //Check if empty
                    completetheform();
                }else if (notes.getText().toString().length() == 0 ) {
                    //Check if empty
                    completetheform();
                }else if ((latitude.toString().length() == 3) && (longitude.toString().length() == 3)) {

                    // create class object
                    gps = new GPSTracker(Diser.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    }

                } else if ((latitude.toString().length() > 3) && (longitude.toString().length() > 3)) {
                    // Converting Edit Text and Text View to String
                    id = combo;
                    nameofbranch = branch.getText().toString();
                    nameofdiser = session.getNAME();
                    position= pos.getText().toString();
                    alias=alias_.getText().toString();
                    sendto = send.getText().toString();
                    emailaddress = email.getText().toString();
                    date = getDate;
                    time = getTime;
                    supervisor = visor.getText().toString();
                    contactperson = contact.getText().toString();
                    subject = sub.getText().toString();
                    address = add.getText().toString();
                    mobilenumber = mob.getText().toString();
                    remarks = notes.getText().toString();
                    gpslatitude = latitude.toString();
                    gpslongitude = longitude.toString();

                    // Calling the function of AsyncReport for registering the data from the Diser Form
                    new Diser.AsyncReport().execute(id,nameofbranch,nameofdiser,position,alias,sendto,emailaddress,supervisor,contactperson,subject,address,mobilenumber,date,time,remarks,gpslatitude,gpslongitude);
                }
            }
        });
        //Declaring Button
        Button logout = (Button) findViewById(R.id.button11);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Diser.this, "Thank you for using this app.", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(Diser.this, Dashboard.class);
                startActivity(intent);
                Diser.this.finish();
            }
        });
    }

    private class AsyncReport extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(Diser.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/disercode.php");
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("itemcode", params[0])
                        .appendQueryParameter("nameofbranch", params[1])
                        .appendQueryParameter("nameofdiser", params[2])
                        .appendQueryParameter("position", params[3])
                        .appendQueryParameter("alias", params[4])
                        .appendQueryParameter("sendto", params[5])
                        .appendQueryParameter("emailaddress", params[6])
                        .appendQueryParameter("supervisor", params[7])
                        .appendQueryParameter("contactperson", params[8])
                        .appendQueryParameter("subject", params[9])
                        .appendQueryParameter("address", params[10])
                        .appendQueryParameter("mobilenumber", params[11])
                        .appendQueryParameter("date", params[12])
                        .appendQueryParameter("time", params[13])
                        .appendQueryParameter("remarks", params[14])
                        .appendQueryParameter("gpslatitude", params[15])
                        .appendQueryParameter("gpslongitude", params[16]);
                String query = builder.build().getEncodedQuery();
                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {
                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            pdLoading.dismiss();
            if (result.equalsIgnoreCase("true")) {
                // Calling success function
                success();
            } else if (result.equalsIgnoreCase("false")) {
                // Calling somethingwentwrong function
                somethingwentwrong();
            } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {
                // Calling error function
                somethingwentwrong();
            }
        }
    }

    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        // Save the text in SharedPreference
                        Intent intent = new Intent(Diser.this, Diser_inventory.class);
                        startActivity(intent);
                        Diser.this.finish();

                    }
                });
        builder.show();
    }

    private void somethingwentwrong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Something when wrong. Please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.messageoferror);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void digitnumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please enter a valid contact number. Thank yyou");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void connectingtoserver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Connecting to server, please wait..");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    //Check if Internet Connection is connectedk if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press next to complete the transaction.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

}

