package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.SessionManager;
import ph.com.applife.myReportNew.SharedPreference;

public class Diser_inventory extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private TextView text1,text2;
    TextView name,iden;
    private EditText itempro,begin, expire, stocks, stockspull, expirestock, deli, end, ware, rdu, bite;
    DatePickerDialog datePickerDialog;
    String itemcode,nameofproduct,beginningInventory,expireDate, solidStocks, stocksforpullout, stockexpiredate, deliveryitem, endingInventory, warehouseStocks, rduStocks, ratBite;
    SessionManager manager;
    SharedPreference sharedPreference;
    String text;
    Activity context = this;
    public Bundle getBundle;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diser_inventory);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        manager = new SessionManager();
        sharedPreference = new SharedPreference();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //Declare Text View
        text1=(TextView)findViewById(R.id.textView29);
        text1.setTypeface(custom_font);
        //Declare Text View
        iden = (TextView) findViewById(R.id.textView28);
        iden.setTypeface(custom_font);
        //Declare Text View
        iden.setText(Diser.combo);
        //Declare Text View
        itempro = (EditText) findViewById(R.id.editText50);
        itempro.setTypeface(custom_font);
        //Declare Text View
        begin = (EditText) findViewById(R.id.editText34);
        begin.setTypeface(custom_font);
        //Declare Text View
        stocks = (EditText) findViewById(R.id.editText39);
        stocks.setTypeface(custom_font);
        //Declare Text View
        stockspull = (EditText) findViewById(R.id.editText40);
        stockspull.setTypeface(custom_font);
        //Declare Text View
        deli = (EditText) findViewById(R.id.editText42);
        deli.setTypeface(custom_font);
        //Declare Text View
        end = (EditText) findViewById(R.id.editText43);
        end.setTypeface(custom_font);
        //Declare Text View
        ware = (EditText) findViewById(R.id.editText45);
        ware.setTypeface(custom_font);
        //Declare Text View
        rdu = (EditText) findViewById(R.id.editText46);
        rdu.setTypeface(custom_font);
        //Declare Text View
        bite = (EditText) findViewById(R.id.editText47);
        bite.setTypeface(custom_font);

        //Declare Edit View
        expire = (EditText) findViewById(R.id.editText37);
        expire.setTypeface(custom_font);
        expire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(Diser_inventory.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                expire.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        //Declare Edit View
        expirestock = (EditText) findViewById(R.id.editText41);
        expirestock.setTypeface(custom_font);
        expirestock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(Diser_inventory.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                expirestock.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        //Declaring Button
        Button button = (Button) findViewById(R.id.button19);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Diser_inventory.this, Add_Item.class);
                startActivity(intent);
                Diser_inventory.this.finish();

            }

        });

        //Declaring Button
        Button button1 = (Button) findViewById(R.id.button11);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Messages if empty
                if (itempro.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (begin.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (expire.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (stocks.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (stockspull.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (expirestock.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (deli.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (end.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (ware.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (rdu.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (bite.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else {

                    // Converting Edit Text and Text View to String
                    itemcode = Diser.combo;
                    nameofproduct = itempro.getText().toString();
                    beginningInventory = begin.getText().toString();
                    expireDate = expire.getText().toString();
                    solidStocks = stocks.getText().toString();
                    stocksforpullout = stockspull.getText().toString();
                    stockexpiredate = expirestock.getText().toString();
                    deliveryitem = deli.getText().toString();
                    endingInventory = end.getText().toString();
                    warehouseStocks = ware.getText().toString();
                    rduStocks = rdu.getText().toString();
                    ratBite = bite.getText().toString();

                    // Calling the function of AsyncReport for registering the data from the Diser_inventory Form
                    new Diser_inventory.AsyncReport().execute(itemcode, nameofproduct, beginningInventory, expireDate, solidStocks, stocksforpullout, stockexpiredate, deliveryitem, endingInventory, warehouseStocks, rduStocks, ratBite);
                }
            }
        });
    }

    private class AsyncReport extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(Diser_inventory.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/disercode_inventory.php");
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);


                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("itemcode", params[0])
                        .appendQueryParameter("nameofproduct", params[1])
                        .appendQueryParameter("beginningInventory", params[2])
                        .appendQueryParameter("expireDate", params[3])
                        .appendQueryParameter("solidStocks", params[4])
                        .appendQueryParameter("stocksforpullout", params[5])
                        .appendQueryParameter("stockexpiredate", params[6])
                        .appendQueryParameter("deliveryitem", params[7])
                        .appendQueryParameter("endingInventory", params[8])
                        .appendQueryParameter("warehouseStocks", params[9])
                        .appendQueryParameter("rduStocks", params[10])
                        .appendQueryParameter("ratBite", params[11]);
                String query = builder.build().getEncodedQuery();
                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {
                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();

            if (result.equalsIgnoreCase("true")) {
                // Calling success function
                success();
            } else if (result.equalsIgnoreCase("1")) {
                // Calling somethingwentwrong function
                somethingwentwrong();
            }  else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {
                // Calling error function
                somethingwentwrong();
            }
        }
    }

    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Intent intent = new Intent(Diser_inventory.this, Diser.class);
                        startActivity(intent);
                        Diser_inventory.this.finish();
                    }
                });
        builder.show();
    }

    private void somethingwentwrong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Something when wrong. Please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setCancelable(false);
        builder.setMessage("Please complete the transaction.");
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Diser_inventory.this, Diser.class);
                        startActivity(intent);
                        Diser_inventory.this.finish();
                    }
                });
        builder.show();

    }
}
