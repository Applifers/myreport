package ph.com.applife.myReportNew;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Splashscreen extends AppCompatActivity {
public int SPLASH_TIME= 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        //Declaring Image View
        ImageView myAnimation = (ImageView)findViewById(R.id.myanimation);
        final AnimationDrawable myAnimationDrawable = (AnimationDrawable)myAnimation.getDrawable();

        myAnimation.post(
                new Runnable(){

                    @Override
                    public void run() {
                        myAnimationDrawable.start();
                        new BackgroundTask().execute();
                    }
                });
    }
    private class BackgroundTask extends AsyncTask {
        Intent splash;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            splash = new Intent(Splashscreen.this, Login.class);
        }
        @Override
        protected Object doInBackground(Object[] params) {
            try {
                Thread.sleep(SPLASH_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            startActivity(splash);
            finish();
        }
    }
}
