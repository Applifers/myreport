package ph.com.applife.myReportNew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import ph.com.applife.myReportNew.reports.TimeCard;

public class Puchase extends AppCompatActivity implements BillingProcessor.IBillingHandler{

    Button buy;
    BillingProcessor bp;
    Button pay;
    private Session session;
    TextView t1, t2;
    boolean paid;
    String HttpUrl = "https://applife.com.ph/ireport/premium.php";
    ProgressDialog progressDialog;

    ParseContent parseContent;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);



        bp = new BillingProcessor(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj7mnQT8Qe21PfPp83PK0zGwuReufAC5vG3W5pJjXaPr8maqGLzi1p0zyE6dbBeRJGQPv0mnHRHgz99b2bYlPiVYyQr3JS+fixz2pCFZEIJ8Qyxbg+ocDLC2sMm3hMfOxApRBc+l8pGBxm60kTv/47ZhjS9yAqphhWTcDwnHGkCFFv7IMgTdhqv578Lgb/b6GbNPmkktuEUFsrijxnfu/6pKGJGvFWi5We9sbfWY55R9bwSvsx1jAR0UmyHjVFz9xPAgqxXd1Id/CYAUSi4Y0xlvYeQCcX9hahmp82IPYvTTQnUnbZ3mm0KMklqFwovryZ4dtBKcUMaj0CYB6fGp2iQIDAQAB", this);
        session = new Session(this);
        parseContent = new ParseContent(this);


        pay = (Button)findViewById(R.id.pay);
        t1 = (TextView)findViewById(R.id.tv1);
        t2 = (TextView)findViewById(R.id.tv2);

        if (session.getpaid()){
            pay.setVisibility(View.GONE);
            t1.setVisibility(View.VISIBLE);
            t2.setVisibility(View.VISIBLE);
        }
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            bp.purchase(Puchase.this,"applifetech.myreport.premium");

            }
        });
    }


    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        session.setPaid(true);
        pay.setVisibility(View.GONE);
        t1.setVisibility(View.VISIBLE);
        t2.setVisibility(View.VISIBLE);
        try {
            sendVote();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent changepassword = new Intent(Puchase.this, Dashboard.class);
        Puchase.this.startActivity(changepassword);
        Puchase.this.finish();


    }
    private void sendVote() throws IOException, JSONException {
        if (!ApplifeUtils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }

        ApplifeUtils.showSimpleProgressDialog(getApplicationContext());
        final HashMap<String, String> map = new HashMap<>();
        map.put("User", session.getUsername());
        map.put("acctype", "true");



        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(HttpUrl);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("newwwss", result);
                onTaskCompleted(result, 1);
            }
        }.execute();
    }


    private void onTaskCompleted(String response,int task) {
        Log.d("responsejson", response.toString());
        ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case 1:
                if (parseContent.isSuccess(response)) {
                    session.setPaid(true);
                    pay.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t2.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),"Thank you for purchasing",Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(getApplicationContext(), parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }

}