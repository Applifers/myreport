package ph.com.applife.myReportNew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class Profile extends Activity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;

    TextView text1,text2,text3,text4,text5,text6,text7;
    EditText friendlyname,email;
    String fullname,username,alias,emailaddress;
    Button btn1,btn2;
    Typeface font;



    ArrayList<String> listItems=new ArrayList<>();
    ArrayAdapter<String> adapter;
    Spinner spin;
    String url;
    InputStream is=null;
    String result=null;
    String line=null;
    HttpURLConnection urlConnection = null;
    String text;
    String [] alias_name;
    Session session;
    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        session = new Session(this);
        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "font/Gothic.ttf");

        //Declare Text View
        text1 = (TextView) findViewById(R.id.fullname);
        text1.setTypeface(custom_font);

        //Checking if the variable if empty or not and use other variable and vice versa

            text1.setText(session.getNAME());

        //Declare Text View
        text2 = (TextView) findViewById(R.id.username);
        text2.setTypeface(custom_font);

        //Checking if the variable if empty or not and use other variable and vice versa
//        if (user_login != null) {
//            text2.setText(user_login);
//            username = user_login;
//        } else //noinspection ConstantConditions,ConstantConditions
//            if (user_login == null) {
            text2.setText(session.getUsername());
            //username = user_register;

        //Declare Text View
        text3 = (TextView) findViewById(R.id.alias);
        text3.setTypeface(custom_font);

        //Checking if the variable if empty or not and use other variable and vice versa
//        if (alias_login != null) {
//            text3.setText(alias_login);
//        } else //noinspection ConstantConditions,ConstantConditions
//            if (alias_login == null) {
            text3.setText(session.getAlias());
        //Declare Text View
        text4 = (TextView) findViewById(R.id.emailaddress);
        text4.setTypeface(custom_font);
        //Checking if the variable if empty or not and use other variable and vice versa
            text4.setText(session.getEMAIL());
        //Declare Text View
        text5 = (TextView) findViewById(R.id.sendto);
        text5.setTypeface(custom_font);
        text5.setText(session.getSENDTO());


        //Declare Edit Text
        friendlyname = (EditText) findViewById(R.id.editText9);
        friendlyname.setTypeface(custom_font);

        email = (EditText) findViewById(R.id.editText10);
        email.setTypeface(custom_font);

        //Declare Button
        btn1 = (Button) findViewById(R.id.add);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (friendlyname.isEnabled()){
                    friendlyname.setEnabled(false);
                    email.setEnabled(false);
                }else {
                    friendlyname.setEnabled(true);
                    email.setEnabled(true);
                }
            }
        });
        //Declare Button
        btn2 = (Button) findViewById(R.id.back);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this, Dashboard.class);
                startActivity(intent);
                Profile.this.finish();
            }
        });

        final List<String> list1 = new ArrayList<String>();

        text = session.getUsername();
        ContentValues values = new ContentValues();
        values.put("1", text);

        try {
            // Enter URL address where your php file resides
            URL url = new URL("http://applife.com.ph/ireport/spinner.php?string1=" + text);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            is = urlConnection.getInputStream();
        } catch (Exception e) {
            Log.e("Fail 1", e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("Fail 2", e.toString());
        }

        try {
            // Getting
            JSONArray JA = new JSONArray(result);
            JSONObject json = null;
            alias_name = new String[JA.length()];
            for (int i = 0; i < JA.length(); i++) {
                json = JA.getJSONObject(i);
                alias_name[i] = json.getString("alias");
            }
            for (int i = 0; i < alias_name.length; i++) {
                list1.add(alias_name[i]);
            }
            spinner();

        } catch (Exception e) {
            Log.e("Fail 3", e.toString());
        }
    }

    public <ViewGroup> void spinner(){
        spin = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, alias_name)
        {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                font = Typeface.createFromAsset(getAssets(),"font/Gothic.ttf");
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(Color.WHITE);
                v.setTextSize(20);
                return v;
            }

            public View getDropDownView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(Color.BLACK);
                v.setTextSize(20);
                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter1);
    }

    public void addEmail(View arg0) {

        fullname= text1.getText().toString();
        username=text2.getText().toString();
        alias= friendlyname.getText().toString();
        emailaddress = email.getText().toString();


        if (fullname.equals("")){
            //Check if empty
            completetheform();
        }else if (username.equals("")){
            //Check if empty
            completetheform();
        }else if (alias.equals("")){
            //Check if empty
            completetheform();
        }else if (emailaddress.equals("")){
            //Check if empty
            completetheform();
        }else {
            // Calling the function of AsyncReport for registering the data from the Agent Form
            new Profile.AsyncReport().execute(fullname, username, alias, emailaddress);
        }
    }

    private class AsyncReport extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(Profile.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/listofeadd.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("fullname", params[0])
                        .appendQueryParameter("username", params[1])
                        .appendQueryParameter("alias", params[2])
                        .appendQueryParameter("emailaddress", params[3]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();

            if(result.equalsIgnoreCase("true")) {
                success();
            }else if (result.equalsIgnoreCase("false")) {
                error();
            }
        }
    }

    public void changeEmail(View arg0) {
        fullname= text1.getText().toString();
        alias= spin.getSelectedItem().toString();
        // Calling the function of AsyncReport for registering the data from the Agent Form
        new Profile.AsyncReport_Upgrade().execute(fullname,alias);
    }

    private class AsyncReport_Upgrade extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(Profile.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {
            try {

                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/ireport/changeemail.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("fullname", params[0])
                        .appendQueryParameter("alias", params[1]);
                String query = builder.build().getEncodedQuery();

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return(result.toString());

                }else{

                    return("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();
            if(result.equalsIgnoreCase("true")) {
                //Calling function emailchange();
                emailchange();
            }else if (result.equalsIgnoreCase("false")) {
                //Calling function error();
                error();
            }
        }
    }

    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage("New email address is successfully save. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Intent intent = new Intent(Profile.this, Profile.class);
                        startActivity(intent);
                        Profile.this.finish();
                    }
                });
        builder.show();
    }

    private void emailchange() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage("update is successfully save. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        session.setlogin(false);
                        Intent intent = new Intent(Profile.this, Login.class);
                        startActivity(intent);
                        Profile.this.finish();
                    }
                });
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.messageoferror);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press back to Dashboard.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
}
