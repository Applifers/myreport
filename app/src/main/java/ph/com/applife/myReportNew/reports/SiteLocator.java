package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.GPSTracker;
import ph.com.applife.myReportNew.ParseContent;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Session;
import ph.com.applife.myReportNew.SiteLocator_images;

@SuppressWarnings("ALL")
public class SiteLocator extends Activity {
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    // GPSTracker class
    GPSTracker gps;
    // declaration for numbers
    Double latitude = 0.0;
    Double longitude = 0.0;
    Session session;
    ParseContent parseContent;
    private TextView name,text1,text3,text4, pos, text5,text6,text7,send,email,alias_;
    EditText  estab, add, popu, rent, term, contact, sub, mob, comp, noted;
    String  getDate, getTime;
    String nameoflocator, position,alias,sendto,emailaddress, date, time, nameofestablishment, address, population, rental, leaseterm, contactperson,subject, mobilenumber, competitors, remarks, gpslatitude, gpslongitude;



    Activity context = this;

    int MY_PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE;

    private Button next;
    Bundle bundle;



    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_locator);

        //Declare "Gothic" font
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        session = new Session(this);

        //Checking the Permission Access
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        ActivityCompat.requestPermissions
                (SiteLocator.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, MY_PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE);


        // Declare Edit Text
        estab = (EditText) findViewById(R.id.editText25);
        estab.setTypeface(custom_font);
        // Declare Edit Text
        add = (EditText) findViewById(R.id.editText26);
        add.setTypeface(custom_font);
        // Declare Edit Text
        popu = (EditText) findViewById(R.id.editText29);
        popu.setTypeface(custom_font);
        // Declare Edit Text
        rent = (EditText) findViewById(R.id.editText30);
        rent.setTypeface(custom_font);
        // Declare Edit Text
        term = (EditText) findViewById(R.id.editText32);
        term.setTypeface(custom_font);
        // Declare Edit Text
        contact = (EditText) findViewById(R.id.operator);
        contact.setTypeface(custom_font);
        // Declare Edit Text
        sub = (EditText) findViewById(R.id.finds);
        sub.setTypeface(custom_font);
        // Declare Edit Text
        mob = (EditText) findViewById(R.id.editText35);
        mob.setTypeface(custom_font);
        // Declare Edit Text
        comp = (EditText) findViewById(R.id.editText36);
        comp.setTypeface(custom_font);
        // Declare Edit Text
        noted = (EditText) findViewById(R.id.editText37);
        noted.setTypeface(custom_font);

        // Declare Text View
        text1=(TextView)findViewById(R.id.textView21);
        text1.setTypeface(custom_font);
        // Declare Text View
        text5= (TextView)findViewById(R.id.textView56);
        text5.setTypeface(custom_font);
        // Declare Text View
        text6=(TextView)findViewById(R.id.textView53);
        text6.setTypeface(custom_font);
        // Declare Text View
        text7=(TextView)findViewById(R.id.textView47);
        text7.setTypeface(custom_font);
        // Declare Text View
        pos= (TextView)findViewById(R.id.textView57);
        pos.setTypeface(custom_font);
        // Declare Text View
        email=(TextView)findViewById(R.id.textView60);
        email.setTypeface(custom_font);
        // Declare Text View
        send=(TextView)findViewById(R.id.textView59);
        send.setTypeface(custom_font);
        // Declare Text View
        alias_=(TextView)findViewById(R.id.textView64);
        alias_.setTypeface(custom_font);

        //Checking if the variable if empty or not and use other variable and vice versa

            email.setText(session.getEMAIL());
        //Checking if the variable if empty or not and use other variable and vice versa
            send.setText(session.getSENDTO());
        //Checking if the variable if empty or not and use other variable and vice versa
            alias_.setText(session.getAlias());
        // Declare Text View
        name = (TextView) findViewById(R.id.textView20);
        name.setTypeface(custom_font);
        name.setText(session.getNAME());

        //Declaring Date (Year-Month-Date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate= (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate= (dateFormat.format(new Date()));

        //Declaring Date (Hour-Minute)
        Date d=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        getTime= (currentDateTimeString);

        //Declare Button
        Button button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SiteLocator.this, "Thank you for using this app.", Toast.LENGTH_LONG).show();
                moveTaskToBack(true);
                Intent intent = new Intent(SiteLocator.this, Dashboard.class);
                startActivity(intent);
                SiteLocator.this.finish();
            }
        });

        //Declare Button
        next = (Button) findViewById(R.id.button9);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check if my internet connection
                isInternetOn();

                if (name.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (estab.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (add.getText().toString().length() == 0) {
                    //Check if empty
                   completetheform();
                } else if (popu.getText().toString().length() == 0) {
                    //Check if empty
                   completetheform();
                } else if (rent.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (term.getText().toString().length() == 0) {
                    //Check if empty
                   completetheform();
                } else if (contact.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (sub.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (mob.getText().toString().length() == 0) {
                    //Check if empty
                    digitnumber();
                }else if (mob.getText().toString().length() > 11 ) {
                    //Check if empty
                    digitnumber();
                } else if (comp.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                }else if (noted.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if ((latitude.toString().length() == 3) && (longitude.toString().length() == 3)) {
                    gps = new GPSTracker(SiteLocator.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    }

                } else if ((latitude.toString().length() > 3) && (longitude.toString().length() > 3)) {

                    // Converting Edit Text and Text View to String
                    nameoflocator = session.getNAME();
                    position= pos.getText().toString();
                    alias=alias_.getText().toString();
                    sendto = send.getText().toString();
                    emailaddress = email.getText().toString();
                    date = getDate;
                    time = getTime;
                    nameofestablishment = estab.getText().toString();
                    address = add.getText().toString();
                    population = popu.getText().toString();
                    rental = rent.getText().toString();
                    leaseterm = term.getText().toString();
                    contactperson = contact.getText().toString();
                    subject = sub.getText().toString();
                    mobilenumber = mob.getText().toString();
                    competitors = comp.getText().toString();
                    remarks = noted.getText().toString();

                    gpslatitude = latitude.toString();
                    gpslongitude = longitude.toString();

                    //Passing variable to other activity
                    bundle = new Bundle();
                    bundle.putString("nameoflocator", nameoflocator);
                    bundle.putString("position", position);
                    bundle.putString("alias", alias);
                    bundle.putString("sendto", sendto);
                    bundle.putString("emailaddress", emailaddress);
                    bundle.putString("date", date);
                    bundle.putString("time", time);
                    bundle.putString("nameofestablishment", nameofestablishment);
                    bundle.putString("address", address);
                    bundle.putString("population", population);
                    bundle.putString("rental", rental);
                    bundle.putString("leaseterm", leaseterm);
                    bundle.putString("contactperson", contactperson);
                    bundle.putString("subject", subject);
                    bundle.putString("mobilenumber", mobilenumber);
                    bundle.putString("competitors", competitors);
                    bundle.putString("remarks", remarks);
                    bundle.putString("gpslatitude", gpslatitude);
                    bundle.putString("gpslongitude", gpslongitude);

                    //Go to SiteLocator_images Activity
                    Intent intent = new Intent(getApplicationContext(), SiteLocator_images.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }

        });
    }

    private void connectingtoserver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Connecting to server, please wait..");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void digitnumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please enter a valid contact number. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please check your Internet connection");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }


    //Check if Internet Connection is connected if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
                 // if connected with internet
                 return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
                error();
                return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press button next complete the transaction.");
        builder.setPositiveButton("OK",null);
        builder.show();
    }
}

