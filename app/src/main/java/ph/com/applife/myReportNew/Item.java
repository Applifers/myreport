package ph.com.applife.myReportNew;

public class Item {

    private int id;
    private String itemcode;
    private String product;
    private String beginninginventory;
    private String expirydate;
    private String solidstocks;
    private String stocksforpullout;
    private String expirydatepullout;
    private String deliveryitems;
    private String endinginventory;
    private String warehouse;
    private String rdustocks;
    private String ratbite;


    public Item(){}

    public Item(String itemcode,String product, String beginninginventory, String expirydate, String solidstocks, String stocksforpullout, String expirydatepullout, String deliveryitems, String endinginventory, String warehouse, String rdustocks, String ratbite) {
        super();
        this.itemcode = itemcode;
        this.product = product;
        this.beginninginventory = beginninginventory;
        this.expirydate = expirydate;
        this.solidstocks = solidstocks;
        this.stocksforpullout = stocksforpullout;
        this.expirydatepullout = expirydatepullout;
        this.deliveryitems = deliveryitems;
        this.endinginventory = endinginventory;
        this.warehouse = warehouse;
        this.rdustocks = rdustocks;
        this.ratbite = ratbite;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getItemcode() {
        return itemcode;
    }
    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getProduct() {
        return product;
    }
    public void setProduct(String product) {
        this.product = product;
    }

    public String getBeginninginventory() {
        return beginninginventory;
    }
    public void setBeginninginventory(String beginninginventory) {
        this.beginninginventory = beginninginventory;
    }

    public String getExpirydate() {
        return expirydate;
    }
    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getSolidstocks() {
        return solidstocks;
    }
    public void setSolidstocks(String solidstocks) {
        this.solidstocks = solidstocks;
    }

    public String getStocksforpullout() {
        return stocksforpullout;
    }
    public void setStocksforpullout(String stocksforpullout) {
        this.stocksforpullout = stocksforpullout;
    }

    public String getExpirydatepullout() {
        return expirydatepullout;
    }
    public void setExpirydatepullout(String expirydatepullout) {
        this.expirydatepullout = expirydatepullout;
    }

    public String getDeliveryitems() {
        return deliveryitems;
    }
    public void setDeliveryitems(String deliveryitems) {
        this.deliveryitems = deliveryitems;
    }

    public String getEndinginventory() {
        return endinginventory;
    }
    public void setEndinginventory(String endinginventory) {
        this.endinginventory = endinginventory;
    }

    public String getWarehouse() {
        return warehouse;
    }
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getRdustocks() {
        return rdustocks;
    }
    public void setRdustocks(String rdustocks) {
        this.rdustocks = rdustocks;
    }

    public String getRatbite() {
        return ratbite;
    }
    public void setRatbite(String ratbite) {
        this.ratbite = ratbite;
    }

    @Override
    public String toString() {
        return "Item [id=" + id + ",itemcode=" + itemcode + ",product=" + product + ", beginninginventory=" + beginninginventory + ", expirydate=" + expirydate +
                ", solidstocks=" + solidstocks +", stocksforpullout=" + stocksforpullout + ", expirydatepullout=" + expirydatepullout +
                ", deliveryitems=" + deliveryitems + ", endinginventory=" + endinginventory + ", warehouse=" + warehouse + ", rdustocks=" + rdustocks +
                " ,ratbite="+ ratbite + "]";
    }
}
