package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;

import ph.com.applife.myReportNew.ApplifeUtils;
import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.DatabaseHelper;
import ph.com.applife.myReportNew.GPSTracker;
import ph.com.applife.myReportNew.Login;
import ph.com.applife.myReportNew.NetworkStateChecker;
import ph.com.applife.myReportNew.ParseContent;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Register;
import ph.com.applife.myReportNew.Session;
import ph.com.applife.myReportNew.VolleySingleton;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class TimeCard extends Activity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT=10000;
    public static final int READ_TIMEOUT=15000;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 3, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1, CAMERA = 2;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    // GPSTracker class
    Session session;
    ParseContent parseContent;
    GPSTracker gps;
    Double latitude = 0.0;
    Double longitude = 0.0;
    TextView Tname,Temail,Talias,Ttoemail,Tsubject,tcode,msgtv,textin,textout;
    EditText mobnumber, locaddress;
    String getDate,getTime;
    String name, email,alias,sendto,subject,mobilenumber,address,date,time,gpslongitude,gpslatitude;
    String emailfrom_login= Login.emailfrom;
    String emailto_login=Login.emailsendto;
    String emailfrom_register= Register.emailfrom;
    String emailto_register=Register.emailsendto;
    String alias_login=Login.friendly_alias;
    String alias_register=Register.friendly_alias;
    public String combo, timeurl;
    String reportID, message;
    private static final String IMAGE_DIRECTORY = "/myreportimage";
    Uri actualUri;
    private boolean haveimage,timein;
    Bitmap bitmap;
    ArrayAdapter<String> adapterUserType;
   // SessionManager manager;
    //private SharedPreference sharedPreference;
    Activity context = this;
    private LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    static Location mLastLocation;
    Typeface font;
    ProgressDialog progressDialog;
    static String random_string, random_string1,formattedDate;
    private String lat,longi;
    private String exact;


//    private DatabaseHelper db;
//    public static final int SYNCED_WITH_SERVER = 1;
//    public static final int NOT_SYNCED_WITH_SERVER = 0;
//    public static final String DATA_SAVED_BROADCAST = "net.simplifiedcoding.datasaved";
//    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_card);




        startLocationUpdates();
        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "font/Gothic.ttf");
        Button timeoout = (Button)findViewById(R.id.button15);
        session = new Session(this);
        parseContent = new ParseContent(this);
        //testing for randomReportID
        //first random
        if (!session.getrandom()){
            rand();
            session.setrandom(true);
        }



        //Checking the Permission Access
      requestStoragePermission();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //Declare Text View
        tcode=(TextView) findViewById(R.id.txtcode);
        tcode.setTypeface(custom_font);
        tcode.setText(combo);
        msgtv = (TextView) findViewById(R.id.textView5);
        //Declare Text View
        Tname = (TextView) findViewById(R.id.timeN);
        Tname.setTypeface(custom_font);
        //Declare Text View
        Tsubject = (TextView) findViewById(R.id.TCtime);
        Tsubject.setTypeface(custom_font);
        //Declare Text View
        Temail = (TextView) findViewById(R.id.emailT);
        Temail.setTypeface(custom_font);
        //Declare Text View
        Talias = (TextView) findViewById(R.id.textView64);
        Talias.setTypeface(custom_font);
        //Declare Text View
        textin = (TextView) findViewById(R.id.textView82);
        textout = (TextView) findViewById(R.id.textView83);
        Ttoemail = (TextView) findViewById(R.id.textView59);
        Ttoemail.setTypeface(custom_font);
        textin.setText(session.getIntime());
        textout.setText(session.getOuttime());
            Temail.setText(session.getEMAIL());
            Ttoemail.setText(session.getSENDTO());
        //Checking if the variable if empty or not and use other variable and vice vers
            Talias.setText(session.getAlias());

        //Declare Text View
        Tname = (TextView) findViewById(R.id.timeN);

        //Retrieve a value from SharedPreference
        Tname.setText(session.getNAME());

        //Declaring Date (Year-Month-Date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate = (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate = (dateFormat.format(new Date()));

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else
            Toast.makeText(this, "Not Connected!", Toast.LENGTH_SHORT).show();



        //Declaring Edit View
        mobnumber = (EditText) findViewById(R.id.reso);
        mobnumber.setTypeface(custom_font);
        //Declaring Edit View
        locaddress = (EditText) findViewById(R.id.txtlocation);
        locaddress.setTypeface(custom_font);
        //Declaring Button
        Button button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to Dashboard Activity
                //manager.setPreferences(TimeCard.this, "status", "0");
                Intent intent = new Intent(TimeCard.this, Dashboard.class);
                startActivity(intent);
                TimeCard.this.finish();
            }
        });

        timeoout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationUpdates();
                settingRequest();
                if (!ApplifeUtils.isNetworkAvailable(TimeCard.this)) {
                    Toast.makeText(TimeCard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (Tname.getText().toString().length() == 0) {
                    //Check if empty
                   // completetheform();
                } else if (mobnumber.getText().toString().length() == 0) {
                    //Check if empty
                    mobnumber.setError("Please input contact number");
                    mobnumber.requestFocus();
                } else if (locaddress.getText().toString().length() == 0) {
                    locaddress.setError("Please input address");
                    locaddress.requestFocus();
                }

//                    }

                 else if ((!lat.equals("") && (!longi.equals("")))) {


                    if(session.getdisable()) {
                        subject = "Time out Card";

                        //new TimeCard.AsyncReport().execute(name, email, alias, sendto, subject, mobilenumber, address, date, time, gpslatitude, gpslongitude, reportID);

                        timeurl = "http://applife.com.ph/ireport/timeoutSelfie.php";
                        requestStoragePermission();
                        takePhotoFromCamera();

                    }

                    else{
                        Toast.makeText(getApplicationContext(), "You must Time-in before you Time-out", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });


    }



    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void timeMein (View arg0) {
        //check if my internet connection
        startLocationUpdates();
        settingRequest();
        if (!ApplifeUtils.isNetworkAvailable(TimeCard.this)) {
            Toast.makeText(TimeCard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (Tname.getText().toString().length() == 0) {
            //Check if empty
            //completetheform();
        } else if (mobnumber.getText().toString().length() == 0) {
            //Check if empty
            mobnumber.setError("Please input contact number");
            mobnumber.requestFocus();
        } else if (locaddress.getText().toString().length() == 0) {
            locaddress.setError("Please input address");
            locaddress.requestFocus();
            //completetheform();


        }  else if ((!lat.equals("") && (!longi.equals("")))) {

           if(!session.getdisable()) {
               subject = "Time in Card";
               //new TimeCard.AsyncReport().execute(name, email, alias, sendto, subject, mobilenumber, address, date, time, gpslatitude, gpslongitude, reportID);
               timeurl = "http://applife.com.ph/ireport/timeinSelfie.php";
               requestStoragePermission();
               takePhotoFromCamera();
           }
           else {
               Toast.makeText(getApplicationContext(), "You must Time-out before you Time-in", Toast.LENGTH_SHORT).show();
           }


        }


        //saveNameToServer();
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)  && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,}, 6666);

    }


    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, CAMERA);

    }
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code READ_REQUEST_CODE.
        // If the request code seen here doesn't match, it's the response to some other intent,
        // and the below code shouldn't run at all.
        super.onActivityResult(requestCode, resultCode, resultData);


        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && resultCode == Activity.RESULT_OK) {

            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.  Pull that uri using "resultData.getData()"
            //actualUri = resultData.getData();
            if (resultData != null) {

                actualUri = resultData.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    //chosen.setImageBitmap(bitmap);
                    //chosen.invalidate();
                    //rotatebutton.setEnabled(true);
                    //haveimage = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
               // haveimage = false;
            }
        }

        if (requestCode == CAMERA && resultCode == RESULT_OK) {

            haveimage = true;
            bitmap = (Bitmap) resultData.getExtras().get("data");
            Date d = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            String currentDateTimeString = sdf.format(d);
            getTime = (currentDateTimeString);

                ImageUploadToServerFunction();
                    //success();
          //  }

        }
        else {
            Toast.makeText(TimeCard.this, "canceled", Toast.LENGTH_SHORT).show();
        }


    }

    private void ImageUploadToServerFunction() {
        if (!ApplifeUtils.isNetworkAvailable(TimeCard.this)) {
            Toast.makeText(TimeCard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        ByteArrayOutputStream byteArrayOutputStreamObject;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        //rotated = Bitmap.createScaledBitmap(rotated, 600, 600, true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(TimeCard.this, "Image is Uploading", "Please Wait", false, false);
            }


            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);
                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();
                // Printing uploading success message coming from server on android app.
                Log.d("newwwss", string1);
                onTaskCompleted(string1,1);

            }
            private void onTaskCompleted(String response,int task) {
                Log.d("responsejson", response.toString());
                progressDialog.dismiss();  //will remove progress dialog
                switch (task) {
                    case 1:
                        if (parseContent.isSuccess(response)) {
                            if (subject.equals("Time out Card")) {
                                session.setdisable(false);
                                rand();
                                session.setrandom(false);
                                mobnumber.setText("");
                                locaddress.setText("");
                                msgtv.setText("");
                                success();
                                textout.setText("Time out" +"\n" + getDate + "\n" +getTime);
                                textin.setText("");
                                session.setOuttime(""+textout.getText().toString());
                                session.setIntime(""+textin.getText().toString());
                            } else {
                                session.setdisable(true);
                                mobnumber.setText("");
                                locaddress.setText("");
                                msgtv.setText("");
                                textin.setText("Time in" +"\n"+ getDate + "\n" +getTime);
                                textout.setText("");
                                session.setIntime(""+textin.getText().toString());
                                session.setOuttime(""+textout.getText(). toString());
                                success();
                            }

                        }else {
                            Toast.makeText(TimeCard.this, "Error Please Try again", Toast.LENGTH_SHORT).show();
                        }
                }
            }


            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClass imageProcessClass = new ImageProcessClass();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("name",session.getNAME());
                HashMapParams.put("email", Temail.getText().toString());
                HashMapParams.put("alias", Talias.getText().toString());
                HashMapParams.put("sendto", Ttoemail.getText().toString());
                HashMapParams.put("subject", subject);
                HashMapParams.put("mobilenumber",mobnumber.getText().toString());
                HashMapParams.put("address",locaddress.getText().toString());
                HashMapParams.put("date",getDate );
                HashMapParams.put("time",getTime);
                HashMapParams.put("def",getSaltString());
                HashMapParams.put("gpslatitude",lat);
                HashMapParams.put("gpslongitude", longi);
                HashMapParams.put("image_path", ConvertImage);
                HashMapParams.put("reportID", session.getComboid());
                String FinalData = imageProcessClass.ImageHttpRequest(timeurl, HashMapParams);
                return FinalData;


            }


        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();




    }





    private void success() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
        builder.show();
    }


    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.messageoferror);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    //Check if Internet Connection is connected
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( (connec != null ? connec.getNetworkInfo(0).getState() : null) == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press back to Dashboard.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        settingRequest();
    }
    private void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(TimeCard.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;
                }
            }

        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

            } else {

                Log.i("Current Location", "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = Double.toString(location.getLatitude());
        longi= Double.toString(location.getLongitude());
        // Toast.makeText(getApplicationContext(), lat + " , " +longi,Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, mss, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        Geocoder geocoder;
        List<Address> addresses;


    }

    private class ImageProcessClass {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();
                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");
                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {


                stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }
    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public void rand(){

        char[] chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 3; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        random_string = sb1.toString();

        //second random
        char[] number = "1234567890".toCharArray();
        StringBuilder sb2 = new StringBuilder();
        Random random2 = new Random();
        for (int i = 0; i < 4; i++) {
            char c2 = number[random2.nextInt(number.length)];
            sb2.append(c2);
        }
        random_string1 = sb2.toString();
        //third random
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MMyyyydd");
        formattedDate = df.format(c.getTime());

        combo = random_string + "-" + random_string1 + "-" + formattedDate;
        session.setComboid(combo);


    }





}


