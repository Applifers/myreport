package ph.com.applife.myReportNew;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Launcher extends Activity {
    TextView login, register;
    Button  btnlogin,btnregister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        //Declare font and accessing Gothic font in font folder
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        //Declare button
        btnlogin =(Button) findViewById(R.id.button1);
        btnlogin.setTypeface(custom_font);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Go to Login Activity
                Intent mainIntent = new Intent(Launcher.this,Login.class);
                Launcher.this.startActivity(mainIntent);
                Launcher.this.finish();
            }
        });

        btnregister =(Button) findViewById(R.id.button2);
        btnregister.setTypeface(custom_font);
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Go to Register Activity
                Intent mainIntent = new Intent(Launcher.this,Register.class);
                Launcher.this.startActivity(mainIntent);
                Launcher.this.finish();
            }
        });
    }

    private Boolean exit = false;
    public void onBackPressed(){
        if (exit) {
            //pressback();
            Toast.makeText(Launcher.this, "Press Back again to Exit", Toast.LENGTH_LONG).show();
        }
        else {
            exit = true;
            //thankyou();
            Toast.makeText(Launcher.this, "Thank you for using MyReport App.", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(a);
                }
            }, 1000);
        }
    }
    private void thankyou() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Thank you for using MyReport App.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void pressback() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Press Back again to Exit");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
}
