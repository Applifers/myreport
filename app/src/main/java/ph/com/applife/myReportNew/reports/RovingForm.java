package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;

import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;


import ph.com.applife.myReportNew.ApplifeUtils;
import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.ParseContent;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Session;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class RovingForm extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    private DrawingView dv;
    private Paint mPaint;
    LinearLayout lay;
    private Bitmap bmp;
    private File file;
    GoogleApiClient mGoogleApiClient;
    Button capture,reset;
    EditText name,status,observ,concern,opname,storeloc,storeimage;
    private ProgressDialog progressDialog;
    static Location mLastLocation;
    ImageView imageView,camshot1,camshot2,camshot3,camshot4,camshot5;
    Session session;
    ParseContent parseContent;
    Dialog d;

    Button cap,res,send;
    LinearLayout signage;
    Bitmap bitmap,bitmap2,bitmap3,bitmap4,bitmap5;
    Uri actualUri;
    private LocationRequest mLocationRequest;
    private String exact;
    String encodedString,encodedString2,encodedString3,encodedString4,encodedString5,mss,lat,longi;
    private boolean haveimage,haveimage2,haveimage3,haveimage4,haveimage5,havesignature;
    public static final int READ_TIMEOUT=15000;
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 3, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1, CAMERA = 2;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roving_form);
        startLocationUpdates();
        imageView = (ImageView) findViewById(R.id.signimage);
        name = (EditText)findViewById(R.id.name);
        status = (EditText)findViewById(R.id.status);
        observ = (EditText)findViewById(R.id.observation);
        concern = (EditText)findViewById(R.id.concern);
        opname = (EditText)findViewById(R.id.opname);
        storeloc = (EditText)findViewById(R.id.storelocation);
        session = new Session(this);
        parseContent = new ParseContent(this);
        name.setText(session.getNAME());
        cap = (Button)findViewById(R.id.back);
        capture = (Button)findViewById(R.id.send);
        reset = (Button) findViewById(R.id.back);
        havesignature=false;
        camshot1 = (ImageView)findViewById(R.id.camshot1);
        camshot2 = (ImageView)findViewById(R.id.camshot2);
        camshot3 = (ImageView)findViewById(R.id.camshot3);
        camshot4 = (ImageView)findViewById(R.id.camshot4);
        camshot5 = (ImageView)findViewById(R.id.camshot5);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                displayNext();
                //Toast.makeText(RovingForm.this,  file.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        camshot1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                takePhotoFromCamera(2);

            }
        });

        camshot2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                takePhotoFromCamera(3);

            }
        });

        camshot3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                takePhotoFromCamera(4);

            }
        });

        camshot4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                takePhotoFromCamera(5);

            }
        });
        camshot5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //captureScreen();
                takePhotoFromCamera(6);

            }
        });






        requestStoragePermission();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }




//this is the button when you click the send button
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (concern.getText().toString().length() == 0 ) {
                    concern.setError("Field is Empty!");
                    concern.requestFocus();
                    return ;
                } else if (observ.getText().toString().length() == 0) {
                    observ.setError("Field is Empty!");
                    observ.requestFocus();
                    return ;
                }  else if (status.getText().toString().length() == 0) {
                    status.setError("Field is Empty!");
                    status.requestFocus();
                    return ;
                } else if (storeloc.getText().toString().length() == 0) {
                    storeloc.setError("Field is Empty!");
                    storeloc.requestFocus();
                    return ;
                }else if (opname.getText().toString().length() == 0 ) {
                    opname.setError("Field is Empty!");
                    opname.requestFocus();
                    return ;
                }

                if (!haveimage) {
                    Snackbar snackbar = Snackbar
                            .make(camshot1, "Must complete all required images", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return ;
                }
                if (!haveimage2) {
                    Snackbar snackbar = Snackbar
                            .make(camshot1, "Must complete all required images", Snackbar.LENGTH_LONG);


                    snackbar.show();

                    return ;
                }
                if (!haveimage3) {
                    Snackbar snackbar = Snackbar
                            .make(camshot1, "Must complete all required images", Snackbar.LENGTH_LONG);
                    snackbar.show();

                    return ;
                }
                if (!haveimage4) {
                    Snackbar snackbar = Snackbar
                            .make(camshot1, "Must complete all required images", Snackbar.LENGTH_LONG);


                    snackbar.show();

                    return ;
                }
                if (!haveimage5) {
                    Snackbar snackbar = Snackbar
                            .make(camshot1, "Must complete all required images", Snackbar.LENGTH_LONG);
                    snackbar.show();

                    return ;
                }

                if (!havesignature) {
                    Snackbar snackbar = Snackbar
                            .make(imageView, "Signature is Empty", Snackbar.LENGTH_LONG)
                            .setAction("Add Signature", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                   displayNext();
                                }
                            });

                    snackbar.show();

                    return ;
                }



                captureScreen();

                //Toast.makeText(RovingForm.this,  file.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RovingForm.this, Dashboard.class);
                startActivity(intent);
                RovingForm.this.finish();

            }
        });
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)  && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,}, 6666);

    }


    private void takePhotoFromCamera(int camera) {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, camera);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code READ_REQUEST_CODE.
        // If the request code seen here doesn't match, it's the response to some other intent,
        // and the below code shouldn't run at all.
        super.onActivityResult(requestCode, resultCode, resultData);


        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && resultCode == Activity.RESULT_OK ) {

            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.  Pull that uri using "resultData.getData()"
            //actualUri = resultData.getData();
            if (resultData != null) {



                actualUri = resultData.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    bitmap2 = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    bitmap3 = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    bitmap4 = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    bitmap5 = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);


                    //chosen.setImageBitmap(bitmap);
                    //chosen.invalidate();
                    //rotatebutton.setEnabled(true);
                    //haveimage = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                haveimage = false;
                haveimage2 = false;
                haveimage3 = false;
                haveimage4 = false;
                haveimage5 = false;
            }
        }

        if (requestCode == 2 && resultCode == RESULT_OK) {

            haveimage = true;
            bitmap = (Bitmap) resultData.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 1000, 1000, true);
            camshot1.setImageBitmap(bitmap);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] byte1 = stream.toByteArray();
            encodedString = Base64.encodeToString(byte1, 0);

        }

        if (requestCode == 3 && resultCode == RESULT_OK) {

            haveimage2 = true;
            bitmap2 = (Bitmap) resultData.getExtras().get("data");
            bitmap2 = Bitmap.createScaledBitmap(bitmap2, 1000, 1000, true);
            camshot2.setImageBitmap(bitmap2);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap2.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] byte1 = stream.toByteArray();
            encodedString2 = Base64.encodeToString(byte1, 0);

        }

        if (requestCode == 4 && resultCode == RESULT_OK) {

            haveimage3 = true;
            bitmap3 = (Bitmap) resultData.getExtras().get("data");
            bitmap3 = Bitmap.createScaledBitmap(bitmap3, 1000, 1000, true);
            camshot3.setImageBitmap(bitmap3);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap3.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] byte1 = stream.toByteArray();
            encodedString3 = Base64.encodeToString(byte1, 0);

        }
        if (requestCode == 5 && resultCode == RESULT_OK) {

            haveimage4 = true;
            bitmap4 = (Bitmap) resultData.getExtras().get("data");
            bitmap4 = Bitmap.createScaledBitmap(bitmap4, 1000, 1000, true);
            camshot4.setImageBitmap(bitmap4);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap4.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] byte1 = stream.toByteArray();
            encodedString4 = Base64.encodeToString(byte1, 0);

        }

        if (requestCode == 6 && resultCode == RESULT_OK) {


            haveimage5 = true;
            bitmap5 = (Bitmap) resultData.getExtras().get("data");
            bitmap5 = Bitmap.createScaledBitmap(bitmap5, 1000, 1000, true);
            camshot5.setImageBitmap(bitmap5);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap5.compress(Bitmap.CompressFormat.PNG, 90, stream);
            byte[] byte1 = stream.toByteArray();
            encodedString5 = Base64.encodeToString(byte1, 0);

        }
        else {
            //Toast.makeText(RovingForm.this, "canceled", Toast.LENGTH_SHORT).show();
        }


    }

    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    private void captureScreen() {
        startLocationUpdates();

        if (!ApplifeUtils.isNetworkAvailable(RovingForm.this)) {
            Toast.makeText(RovingForm.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }

        View v = signage;
        v.setDrawingCacheEnabled(true);
        bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        file = new File(Environment
                .getExternalStorageDirectory().toString(), "SCREEN"
                + System.currentTimeMillis() + ".png");
        //Log.e("here", "------------" + file);
        //FileOutputStream fos = new FileOutputStream(file);
        ByteArrayOutputStream byteArrayOutputStreamObject;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);


        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(RovingForm.this, "Image is Uploading", "Please Wait", false, false);
            }


            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);
                // Dismiss the progress dialog after done uploading.
                progressDialog.dismiss();
                // Printing uploading success message coming from server on android app.
                Log.d("newwwss", string1);
                onTaskCompleted(string1, 1);

            }

            private void onTaskCompleted(String response, int task) {
                Log.d("responsejson", response);
                progressDialog.dismiss();  //will remove progress dialog
                switch (task) {
                    case 1:
                        if (parseContent.isSuccess(response)) {

                            Toast.makeText(RovingForm.this, "Successfully Sent", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RovingForm.this, RovingForm.class);
                            startActivity(intent);
                            RovingForm.this.finish();

                          } else {
                            Snackbar snackbar = Snackbar
                                    .make(camshot1, "Error Please Try again", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                }
            }




            @Override
            protected String doInBackground(Void... params) {

                ImageProcessClass imageProcessClass = new ImageProcessClass();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("name",session.getNAME());
                HashMapParams.put("concern", concern.getText().toString()); //findings
                HashMapParams.put("status", status.getText().toString());
                HashMapParams.put("observe", observ.getText().toString());
                HashMapParams.put("opname", opname.getText().toString());
                HashMapParams.put("storeloc", storeloc.getText().toString());
                HashMapParams.put("camshot", encodedString);
                HashMapParams.put("camshot2", encodedString2);
                HashMapParams.put("camshot3", encodedString3);
                HashMapParams.put("camshot4", encodedString4);
                HashMapParams.put("camshot5", encodedString5);
                HashMapParams.put("gpslat", ""+lat);
                HashMapParams.put("gpslongi",""+ longi);
                HashMapParams.put("location", exact);
                HashMapParams.put("image_path", ConvertImage);
                HashMapParams.put("rand",getSaltString());
                HashMapParams.put("rand2",getSaltString());
                HashMapParams.put("rand3",getSaltString());
                HashMapParams.put("rand4",getSaltString());
                HashMapParams.put("rand5",getSaltString());
                HashMapParams.put("def",getSaltString());
                HashMapParams.put("reportID", session.getComboid());
                HashMapParams.put("sendto", session.getSENDTO());
                String FinalData = imageProcessClass.ImageHttpRequest("http://applife.com.ph/ireport/roving2.php", HashMapParams);
                return FinalData;


            }


        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();

        AsyncTaskUploadClassOBJ.execute();


    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 8) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }



    @Override
    public void onBackPressed() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("EXIT");
        builder.setMessage("Do you want to discard and go back to dashboard? ");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RovingForm.super.onBackPressed();
                Intent intent = new Intent(RovingForm.this, Dashboard.class);
                startActivity(intent);
                RovingForm.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();





    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    private void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(RovingForm.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;
                }
            }

        });
    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

            } else {

                Log.i("Current Location", "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        lat = Double.toString(location.getLatitude());
        longi= Double.toString(location.getLongitude());
        // Toast.makeText(getApplicationContext(), lat + " , " +longi,Toast.LENGTH_SHORT).show();

        //mss = ""+lat+","+longi;
        //Toast.makeText(this, lat, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        Geocoder geocoder;
        List<Address> addresses;

        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                exact = addresses.get(0).getAddressLine(0);
                //Toast.makeText(getApplicationContext(), exact, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("tag", e.getMessage());
        }
    }
    private class ImageProcessClass {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();
                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");
                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {


                stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }
    public class DrawingView extends View {

        public int width;
        public int height;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;
        Context context;
        private Paint circlePaint;
        private Path circlePath;

        public DrawingView(Context c) {
            super(c);
            context = c;
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            circlePaint = new Paint();
            circlePath = new Path();
            circlePaint.setAntiAlias(true);
            circlePaint.setColor(Color.BLACK);
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setStrokeJoin(Paint.Join.MITER);
            circlePaint.setStrokeWidth(2f);
        }



    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
            canvas.drawPath(mPath, mPaint);
            canvas.drawPath(circlePath, circlePaint);
        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;

                circlePath.reset();
                circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            circlePath.reset();
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;//and save as imagend save as image
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }


    }
    private void displayNext() {

        d = new Dialog(this);
        d.setContentView(R.layout.signature);
        signage = (LinearLayout) d.findViewById(R.id.signage);
        dv = new DrawingView(RovingForm.this);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(8);
        signage.addView(dv);


        Button Confirm = (Button) d.findViewById(R.id.send);
        Button reset = (Button) d.findViewById(R.id.back2);
        Confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v = signage;
                v.setDrawingCacheEnabled(true);
                bmp = Bitmap.createBitmap(v.getDrawingCache());
                v.setDrawingCacheEnabled(false);
                file = new File(Environment
                        .getExternalStorageDirectory().toString(), "SCREEN"
                        + System.currentTimeMillis() + ".png");
                //Log.e("here", "------------" + file);
                //FileOutputStream fos = new FileOutputStream(file);
                ByteArrayOutputStream byteArrayOutputStreamObject;
                byteArrayOutputStreamObject = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStreamObject);
                havesignature=true;
                imageView.setImageBitmap(bmp);
                d.dismiss();

            }


        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            d.dismiss();
            displayNext();
            }


        });

        d.show();

    }

}

