package ph.com.applife.myReportNew.reports;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ph.com.applife.myReportNew.Dashboard;
import ph.com.applife.myReportNew.GPSTracker;
import ph.com.applife.myReportNew.ParseContent;
import ph.com.applife.myReportNew.R;
import ph.com.applife.myReportNew.Session;

public class SalesReport extends Activity {
    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    // GPSTracker class
    GPSTracker gps;
    // declaration for numbers
    Double latitude = 0.0;
    Double longitude = 0.0;
    Session session;
    ParseContent parseContent;
    private TextView txtname, txtemail, txtalias, txtsendto, tcode, transdate;
    EditText branch,phone,amount,depoAmount, transNum,remarks;
    String getDate,getTime;
    String nameofuser, alias,sendto,emailaddress,date,time,nameofestablishment,mobilenumber,totalSales, depositAmount, transNumber, transDate, gpslatitude, gpslongitude,reMarks;

    DatePickerDialog datePickerDialog;


    Activity context = this;

    int MY_PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE;

    private Button next;
    Bundle bundle;


    @SuppressWarnings("ConstantConditions")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_report);

        //Declare "Gothic" font
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");

        session = new Session(this);
        parseContent = new ParseContent(this);
        //Checking the Permission Access
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        ActivityCompat.requestPermissions
                (SalesReport.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, MY_PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE);

        branch=(EditText)findViewById(R.id.edtBranch);
        branch.setTypeface(custom_font);
        phone=(EditText)findViewById(R.id.edtPhone);
        phone.setTypeface(custom_font);
        amount=(EditText)findViewById(R.id.txtAmount);
        amount.setTypeface(custom_font);
        depoAmount=(EditText)findViewById(R.id.txtdepoAmount);
        depoAmount.setTypeface(custom_font);
        transNum=(EditText)findViewById(R.id.txttransAmount);
        transNum.setTypeface(custom_font);
        remarks=(EditText)findViewById(R.id.edtRemark);
        remarks.setTypeface(custom_font);
        transdate=(TextView)findViewById(R.id.edtTransDate);
        transdate.setTypeface(custom_font);
        transdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(SalesReport.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                transdate.setText(year + "-"
                                        + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        //TextView

        txtemail=(TextView)findViewById(R.id.textView60);
        txtemail.setTypeface(custom_font);
        txtalias=(TextView)findViewById(R.id.textView64);
        txtalias.setTypeface(custom_font);
        txtsendto=(TextView)findViewById(R.id.textView59);
        txtsendto.setTypeface(custom_font);
        //

                txtemail.setText(session.getEMAIL());
        //Checking if the variable if empty or not and use other variable and vice versa
                txtsendto.setText(session.getSENDTO());
        //Checking if the variable if empty or not and use other variable and vice versa
                txtalias.setText(session.getAlias());
        txtname=(TextView)findViewById(R.id.textView58);
        txtname.setTypeface(custom_font);
        txtname.setText(session.getNAME());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate= (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate= (dateFormat.format(new Date()));

        //Declaring Date (Hour-Minute)
        Date d=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        getTime= (currentDateTimeString);

        //Declare Button
        Button button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SiteLocator.this, "Thank you for using this app.", Toast.LENGTH_LONG).show();
                moveTaskToBack(true);
                Intent intent = new Intent(SalesReport.this, Dashboard.class);
                startActivity(intent);
                SalesReport.this.finish();
            }
        });

        next=(Button)findViewById(R.id.button5);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check if my internet connection
                isInternetOn();


                if (txtname.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (branch.getText().toString().length() == 0) {
                    //Check if empty
                    completetheform();
                } else if (phone.getText().toString().length() == 0) {
                    completetheform();
                } else if (amount.getText().toString().length()==0){
                    completetheform();
                } else if (phone.getText().toString().length()>11){
                    digitnumber();
                } else if (depoAmount.getText().toString().length()==0){
                    completetheform();
                } else if (transNum.getText().toString().length()==0){
                    completetheform();
                } else if (transdate.getText().toString().length()==0){
                    completetheform();
                }else if ((latitude.toString().length() == 3) && (longitude.toString().length() == 3)) {
                    gps = new GPSTracker(SalesReport.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        connectingtoserver();
                    }

                } else if ((latitude.toString().length() > 3) && (longitude.toString().length() > 3)) {
                    nameofuser=session.getNAME();
                    alias=txtalias.getText().toString();
                    sendto=txtsendto.getText().toString();
                    emailaddress=txtemail.getText().toString();
                    date=getDate;
                    time=getTime;
                    nameofestablishment=branch.getText().toString();
                    mobilenumber=phone.getText().toString();
                    totalSales= amount.getText().toString();
                    depositAmount=depoAmount.getText().toString();
                    transNumber=transNum.getText().toString();
                    transDate=transdate.getText().toString();
                    gpslatitude = latitude.toString();
                    gpslongitude = longitude.toString();
                    reMarks=remarks.getText().toString();

                    //Passing variable to other activity
                    bundle = new Bundle();
                    bundle.putString("nameofuser", nameofuser);
                    bundle.putString("alias", alias);
                    bundle.putString("sendto", sendto);
                    bundle.putString("emailaddress", emailaddress);
                    bundle.putString("date", date);
                    bundle.putString("time", time);
                    bundle.putString("nameofestablishment", nameofestablishment);
                    bundle.putString("mobilenumber",mobilenumber);
                    bundle.putString("totalSales",totalSales);
                    bundle.putString("depositAmount", depositAmount);
                    bundle.putString("transNumber", transNumber);
                    bundle.putString("transDate", transDate);
                    bundle.putString("gpslatitude", gpslatitude);
                    bundle.putString("gpslongitude", gpslongitude);
                    bundle.putString("remarks",reMarks);

                    //Go to SiteLocator_images Activity
                    Intent intent = new Intent(getApplicationContext(), SalesReportImg.class);
                    intent.putExtras(bundle);
                    startActivity(intent);


                }
            }
        });

    }
    private void connectingtoserver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Connecting to server, please wait..");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
    private void digitnumber() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please enter a valid contact number. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please check your Internet connection");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    //Check if Internet Connection is connected if Internet Connection is connected
    @SuppressWarnings("ConstantConditions")
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        //noinspection ConstantConditions
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            // if connected with internet
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press button Add Photo complete the transaction.");
        builder.setPositiveButton("OK",null);
        builder.show();
    }

}
