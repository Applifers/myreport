package ph.com.applife.myReportNew;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    //Constants for Database name, table name, and column names
    public static final String DB_NAME = "Iimeinoffline";
    public static final String TABLE_NAME = "timein";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_STATUS = "status";
//    public static final String COLUMN_EMAIL = "email";
//    public static final String COLUMN_ALIAS = "alias";
//    public static final String COLUMN_SENDTO = "sendto";
//    public static final String COLUMN_SUBJECT = "subject";
//    public static final String COLUMN_MOBILENUMBER = "mobilenumber";
//    public static final String COLUMN_ADDRESS = "address";
//    public static final String COLUMN_DATE = "date";
//    public static final String COLUMN_TIME = "time";
//    public static final String COLUMN_LATITUDE = "latitude";
//    public static final String COLUMN_LONGTITUDE = "longtitude";
//    public static final String COLUMN_REPORTID = "reportid";
//    public static final String COLUMN_TIMEINIMAGE = "timeinimage";


    //database version
    private static final int DB_VERSION = 1;

    //Constructor
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //creating the database
    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String sql = "CREATE TABLE " + TABLE_NAME
//                + "(" + COLUMN_ID +
//                " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME +
//                " VARCHAR, " + COLUMN_EMAIL +
//                " VARCHAR," + COLUMN_ALIAS +
//                " VARCHAR," + COLUMN_SENDTO +
//                " VARCHAR," + COLUMN_SUBJECT +
//                " VARCHAR," + COLUMN_MOBILENUMBER +
//                " VARCHAR," + COLUMN_ADDRESS +
//                " DATE," + COLUMN_DATE +
//                " VARCHAR," + COLUMN_TIME +
//                " VARCHAR," + COLUMN_LATITUDE +
//                " VARCHAR," + COLUMN_LONGTITUDE +
//                " VARCHAR," + COLUMN_REPORTID +
//                " VARCHAR," + COLUMN_TIMEINIMAGE +
//                " VARCHAR," + COLUMN_STATUS +
//                " TINYINT);";
//        db.execSQL(sql);
//    }

    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME
                + "(" + COLUMN_ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME +
                " VARCHAR, " + COLUMN_STATUS +
                " TINYINT);";
        db.execSQL(sql);
    }

    //upgrading the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Persons";
        db.execSQL(sql);
        onCreate(db);
    }

    /*
     * This method is taking two arguments
     * first one is the name that is to be saved
     * second one is the status
     * 0 means the name is synced with the server
     * 1 means the name is not synced with the server
     * */
//    public boolean addtimein(String name, String timein, String email, String alias, String sendto, String subject, int mobilenumber, String address , String date, String latitude, String longtitude, String reportid, String timeinimage, int status ) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//
//        contentValues.put(COLUMN_NAME, name);
//        contentValues.put(COLUMN_EMAIL, email);
//        contentValues.put(COLUMN_TIME, timein);
//        contentValues.put(COLUMN_ALIAS, alias);
//        contentValues.put(COLUMN_SENDTO, sendto);
//        contentValues.put(COLUMN_SUBJECT, subject);
//        contentValues.put(COLUMN_MOBILENUMBER, mobilenumber);
//        contentValues.put(COLUMN_ADDRESS, address);
//        contentValues.put(COLUMN_DATE, date);
//        contentValues.put(COLUMN_LATITUDE, latitude);
//        contentValues.put(COLUMN_LONGTITUDE, longtitude);
//        contentValues.put(COLUMN_REPORTID, reportid);
//        contentValues.put(COLUMN_TIMEINIMAGE, timeinimage);
//        contentValues.put(COLUMN_STATUS, status);
//
//
//
//        db.insert(TABLE_NAME, null, contentValues);
//        db.close();
//        return true;
//    }


    public boolean addtimein(String name,int status ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_STATUS, status);



        db.insert(TABLE_NAME, null, contentValues);
        db.close();
        return true;
    }

    /*
     * This method taking two arguments
     * first one is the id of the name for which
     * we have to update the sync status
     * and the second one is the status that will be changed
     * */
    public boolean updateNameStatus(int id, int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_STATUS, status);
        db.update(TABLE_NAME, contentValues, COLUMN_ID + "=" + id, null);
        db.close();
        return true;
    }

    /*
     * this method will give us all the name stored in sqlite
     * */
    public Cursor getNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COLUMN_ID + " ASC;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    /*
     * this method is for getting all the unsynced name
     * so that we can sync it with database
     * */
    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_STATUS + " = 0;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
}