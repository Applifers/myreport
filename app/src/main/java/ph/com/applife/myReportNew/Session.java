package ph.com.applife.myReportNew;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences prefs;
    // SharedPreferences.Editor editor;
    Context ctx;
    private final String Username = "user";
    private final String EMAIL = "email";
    private final String Password = "password";
    private final String NAME = "name";
    private final String SENDTO = "sendto";
    private final String Alias = "alias";
    private final String comboid = "reportid";
    private final String intime = "intime";
    private final String outtime = "outtime";





    public Session(Context ctx) {

        prefs = ctx.getSharedPreferences("myapp", Context.MODE_PRIVATE);
        this.ctx = ctx;
        //editor = prefs.edit();
    }


    public void setPaid(boolean paid) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("paid", paid);
        editor.commit();
    }

    public boolean getpaid() {
        return prefs.getBoolean("paid", false);
    }

    public void setrandom(boolean random) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("random", random);
        editor.commit();
    }

    public boolean getrandom() {
        return prefs.getBoolean("random", false);
    }




    public void setdisable (boolean disable) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean("disable", disable);
        edit.commit();
    }

    public boolean getdisable() {
        return prefs.getBoolean("disable", false);
    }

    public void setlogin(boolean login) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("login", login);
        editor.commit();
    }

    public boolean getlogin() {
        return prefs.getBoolean("login", false);
    }


    public void setUsername(String user) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(Username, user);
        edit.commit();
    }
    public String getUsername (){

        return prefs.getString(Username,"");
    }

    public void setComboid(String reportid) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(comboid, reportid);
        edit.commit();
    }
    public String getComboid (){

        return prefs.getString(comboid,"");
    }

    public void setEMAIL(String email) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(EMAIL, email);
        edit.commit();
    }
    public String getEMAIL (){

        return prefs.getString(EMAIL,"");
    }

    public void setIntime(String email) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(intime, email);
        edit.commit();
    }
    public String getIntime (){

        return prefs.getString(intime,"");
    }
    public void setOuttime(String email) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(outtime, email);
        edit.commit();
    }
    public String getOuttime (){

        return prefs.getString(outtime,"");
    }

    public void setNAME(String name) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(NAME, name);
        edit.commit();
    }
    public String getNAME (){

        return prefs.getString(NAME,"");
    }

    public void setPassword(String password) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(Password, password);
        edit.commit();
    }
    public String getPassword (){

        return prefs.getString(Password,"");
    }


    public void setSENDTO(String sendto) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(SENDTO, sendto);
        edit.commit();
    }
    public String getSENDTO (){

        return prefs.getString(SENDTO,"");
    }


    public void setAlias(String alias) {
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(Alias, alias);
        edit.commit();
    }
    public String getAlias (){

        return prefs.getString(Alias,"");
    }

}


