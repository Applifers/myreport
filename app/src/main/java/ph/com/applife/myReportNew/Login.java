package ph.com.applife.myReportNew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class Login extends AppCompatActivity {


  // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
  public static final int CONNECTION_TIMEOUT=30000;
  public static final int READ_TIMEOUT=15000;
  public static String  full;
  public static String  friendly_alias;
  public static String  emailsendto;
  public static String  emailfrom;
  public static String  user_;

  private EditText user,pass;
  private static final String TAG_fullname = "fullname";
  private static final String TAG_email = "email";
  private static final String TAG_username = "username";
  private static final String TAG_sendto = "sendto";
  private static final String TAG_alias = "alias";
  String fullname,username,password,email,sendto,position,alias;
  public TextView forgot;
  SessionManager manager;
  private SharedPreference sharedPreference;
  Activity context = this;
  private String text;
  Typeface font;
  Button signup;
  Session session;
  ParseContent parseContent;
  String login= "http://applife.com.ph/ireport/nlogin.php";
  AdView mAdView;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    //Declare font and accessing Gothic font in font folder
    Typeface custom_font = Typeface.createFromAsset(getAssets(),  "font/Gothic.ttf");


    session = new Session(this);
    parseContent = new ParseContent(this);
    // Declare Edit Text


    mAdView = (AdView) findViewById(R.id.adView);
    AdRequest adRequest = new AdRequest.Builder().build();
    mAdView.loadAd(adRequest);


    user = (EditText) findViewById(R.id.username);
    user.setTypeface(custom_font);
    pass = (EditText) findViewById(R.id.password);
    signup = (Button) findViewById(R.id.button12);
    pass.setTypeface(custom_font);
    if (session.getlogin()){
      Intent mainIntent = new Intent(Login.this,Dashboard.class);
      Login.this.startActivity(mainIntent);
      Login.this.finish();
    }

    forgot=(TextView) findViewById(R.id.forgotPass);
    forgot.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v){
        Intent forg;
        forg = new Intent(Login.this, forotP.class);
        startActivity(forg);
        Login.this.finish();
      }
    });

    signup.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent mainIntent = new Intent(Login.this,Register.class);
        Login.this.startActivity(mainIntent);
        Login.this.finish();
      }
    });




  }

  // Triggers when LOGIN Button clicked
  public void checkLogin(View arg0) {
    //check if my internet connection
    isInternetOn();

    // Converting Edit Text and Text View to String
    username = user.getText().toString();
    password = pass.getText().toString();


    if (username.equals("")){
      //Check if empty
      //Toast.makeText(Login.this, "User name is empty.", Toast.LENGTH_LONG).show();
      completetheform();
    }else if (password.equals("")){
      //Check if empty
      //Toast.makeText(Login.this, "Password is empty.", Toast.LENGTH_LONG).show();
      completetheform();
    }else {
      // Calling the function of AsyncLogin to login the data from the Login Form
      try {
        login();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (JSONException e) {
        e.printStackTrace();
      }

    }


  }

  private void login() throws IOException, JSONException {
    if (!ApplifeUtils.isNetworkAvailable(Login.this)) {
      Toast.makeText(Login.this, "Internet is required!", Toast.LENGTH_SHORT).show();
      return;
    }
    ApplifeUtils.showSimpleProgressDialog(Login.this);
    final HashMap<String, String> map = new HashMap<>();
    map.put("username",username);
    map.put("password", password);
    new AsyncTask<Void, Void, String>(){
      protected String doInBackground(Void[] params) {
        String response="";
        try {
          HttpRequest req = new HttpRequest(login);
          response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
        } catch (Exception e) {
          response=e.getMessage();
        }
        return response;
      }
      protected void onPostExecute(String result) {
        //do something with response
        Log.d("newwwss", result);
        onTaskCompleted(result,1);
      }
    }.execute();
  }

  private void onTaskCompleted(String response,int task) {
    Log.d("responsejson", response.toString());
    ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
    switch (task) {
      case 1:
        if (parseContent.isSuccess(response)) {
          session.setlogin(true);
          parseContent.saveInfo(response);
          Toast.makeText(Login.this, "Login Successfully!", Toast.LENGTH_SHORT).show();
          Intent intent = new Intent(Login.this,Dashboard.class);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
          startActivity(intent);
          this.finish();
        }else {
          Toast.makeText(Login.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }
  }










//for forgot password code


  //    private class AsyncLogin extends AsyncTask<String, String, String>
//    {
//        ProgressDialog pdLoading = new ProgressDialog(Login.this);
//        HttpURLConnection conn;
//        URL url = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
//
//        }
//        @Override
//        protected String doInBackground(String... params) {
//
//            try {
//
//                // Enter URL address where your php file resides
//                url = new URL("http://applife.com.ph/ireport/login.php");
//
//            } catch (MalformedURLException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//                return "exception";
//            }
//            try {
//                // Setup HttpURLConnection class to send and receive data from php and mysql
//                conn = (HttpURLConnection)url.openConnection();
//                conn.setReadTimeout(READ_TIMEOUT);
//                conn.setConnectTimeout(CONNECTION_TIMEOUT);
//                conn.setRequestMethod("POST");
//                // setDoInput and setDoOutput method depict handling of both send and receive
//                conn.setDoInput(true);
//                conn.setDoOutput(true);
//
//                // Append parameters to URL
//                Uri.Builder builder = new Uri.Builder()
//                        .appendQueryParameter("username", params[0])
//                        .appendQueryParameter("password", params[1]);
//                String query = builder.build().getEncodedQuery();
//                // Open connection for sending data
//                OutputStream os = conn.getOutputStream();
//                BufferedWriter writer = new BufferedWriter(
//                        new OutputStreamWriter(os, "UTF-8"));
//                writer.write(query);
//                writer.flush();
//                writer.close();
//                os.close();
//                conn.connect();
//            } catch (IOException e1) {
//                // TODO Auto-generated catch block
//                e1.printStackTrace();
//                return "exception";
//            }
//            try {
//                int response_code = conn.getResponseCode();
//                // Check if successful connection made
//                if (response_code == HttpURLConnection.HTTP_OK) {
//                    // Read data sent from server
//                    InputStream input = conn.getInputStream();
//                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
//                    StringBuilder result = new StringBuilder();
//                    String line;
//                    while ((line = reader.readLine()) != null) {
//                        result.append(line);
//                    }
//                    // Pass data to onPostExecute method
//                    return(result.toString());
//                }else{
//                    return("unsuccessful");
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//                return "exception";
//            } finally {
//                conn.disconnect();
//            }
//        }
//        protected void onPostExecute(String result) {
//            pdLoading.dismiss();
//            //this method will be running on UI thread
//            if(result.equalsIgnoreCase("false"))
//            {
//                noresult();
//            }else {
//                try {
//
//                    JSONArray jArray = new JSONArray(result);
//
//                    // Extract data from json and store into ArrayList as class objects
//                    for (int i = 0; i < jArray.length(); i++) {
//                        JSONObject c = jArray.getJSONObject(i);
//                        fullname = c.getString(TAG_fullname);
//                        email = c.getString(TAG_email);
//                        username = c.getString(TAG_username);
//                        sendto = c.getString(TAG_sendto);
//                        alias = c.getString(TAG_alias);
//                    }
//
//                    full = fullname;
//                    emailfrom = email;
//                    friendly_alias = alias;
//                    emailsendto = sendto;
//                    user_ = username;
//
//                    manager.setPreferences(Login.this, "status", "1");
//                    text=fullname;
//                    String status=manager.getPreferences(Login.this,"status");
//                    Log.d("status", status);
//
//                    // Save the text in SharedPreference
//                    sharedPreference.save(context,text);
//                    session.setlogin(true);
//                    Intent intent = new Intent(Login.this, Dashboard.class);
//                    startActivity(intent);
//                    Login.this.finish();
//
//                }  catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//
//    }
//
//
//    }
  private void noresult() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
    builder.setTitle("Alert Message:");
    builder.setIcon( android.R.drawable.stat_notify_error);
    builder.setMessage("Invalid username or password. Try Again!");
    builder.setCancelable(false);
    builder.setPositiveButton("OK",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog,
                                  int which) {

              }
            });
    builder.show();
  }

  private void somethingwentwrong() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
    builder.setTitle(R.string.title);
    builder.setIcon( android.R.drawable.stat_notify_error);
    builder.setMessage("Something when wrong. Please try again.");
    builder.setCancelable(false);
    builder.setPositiveButton("OK",null);
    builder.show();
  }

  private void completetheform() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
    builder.setTitle("Alert Message:");
    builder.setIcon( android.R.drawable.stat_notify_error);
    builder.setMessage("Please complete the login details to continue. Thank you.");
    builder.setCancelable(false);
    builder.setPositiveButton("OK",
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog,
                                  int which) {
                Intent intent = new Intent(Login.this, Login.class);
                startActivity(intent);
                Login.this.finish();
              }
            });
    builder.show();
  }

  private void error() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
    builder.setTitle(R.string.title);
    builder.setIcon( android.R.drawable.ic_dialog_alert);
    builder.setMessage("Please check your Internet connection");
    builder.setCancelable(false);
    builder.setPositiveButton("OK",null);
    builder.show();
  }

  //Check if Internet Connection is connected
  @SuppressWarnings("ConstantConditions")
  public final boolean isInternetOn() {
    // get Connectivity Manager object to check connection
    ConnectivityManager connec =
            (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

    // Check for network connections
    //noinspection ConstantConditions
    if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
            connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
            connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
            connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
      // if connected with internet
      return true;

    } else if (
            connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                    connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
      error();
      return false;
    }
    return false;
  }


  @Override
  public void onBackPressed() {
    // Write your code here
    Intent mainIntent = new Intent(Login.this,Launcher.class);
    Login.this.startActivity(mainIntent);
    Login.this.finish();
  }
}